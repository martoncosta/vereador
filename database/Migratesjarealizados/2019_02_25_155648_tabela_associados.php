<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaAssociados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dtnascimento');
            $table->string('rg',20);
            $table->string('estadocivil',20);
            $table->string('cpf',20);
            $table->string('naturalidade',50);
            $table->string('nacionalidade',50);
            $table->string('titulo',50);
            $table->string('nome',50);
            $table->string('pai',50);
            $table->string('mae',50);
            $table->string('foto',100);
            $table->string('endereco',50);
            $table->string('bairro',50);
            $table->string('cidade',50);
            $table->string('cep',9);
            $table->string('email',50);
            $table->string('telefone',20);
            $table->string('celular',20);
            $table->string('instrucao',50);
            $table->string('profissao',50);
            $table->string('estaempregado',10);
            $table->string('cursos',200);
            $table->string('gostadefazer',200);
            $table->string('experiencia',200);
            $table->string('porquevoluntario',200);
            $table->string('publicoatrabalhar',200);
            $table->string('horariodisponivel',200);
            $table->string('diassemana',200);
            $table->integer('cargahoraria');
            $table->string('observacoes',300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados');
    }
}
