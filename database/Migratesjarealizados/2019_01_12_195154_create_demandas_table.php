<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Demands', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps('databertura');
            $table->timestamps('datafechamento');
            $table->integer('status');
            $table->integer('categoria');
            $table->integer('subcategoria');
            $table->string('descricao',200);
            $table->string('endereco',60);
            $table->string('bairro',40);
            $table->integer('prazo');
            $table->integer('eleitor');
            $table->integer('solicitante');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Demands');
    }
}
