<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/home', 'Site\HomeController@index');

Route::get('/',['as'=>'site.home','uses'=>'Site\HomeController@index']);
Route::get('/login',['as'=>'site.login','uses'=>'Site\LoginController@index']);
Route::post('/login/entrar',['as'=>'site.login.entrar','uses'=>'Site\LoginController@entrar']);

Route::get('/login/resetar',['as'=>'site.login.resetar','uses'=>'Site\LoginController@resetar']);
Route::get('/login/sair',['as'=>'site.login.sair','uses'=>'Site\LoginController@sair']);

/* Resetar a Senha */
Route::get('/changePassword','Site\LoginController@showChangePasswordForm');
Route::post('/changePassword','Site\LoginController@changePassword')->name('changePassword');

Route::get('auth/passwords/reset',['as'=>'auth.passwords.reset','uses'=>'Site\LoginController@resetPassword']);
Route::get('auth/passwords/email',['as'=>'auth.passwords.email','uses'=>'Site\LoginController@emailPassword']);
Route::get('auth/passwords/password',['as'=>'auth.passwords.password','uses'=>'Site\LoginController@sendPassword']);

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para o calendario */
Route::get('admin/calendario',['as'=>'changePassword1','uses'=>'Admin\CalendarioController@index']);
});


/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Assessores */
Route::get('admin/assessores',['as'=>'admin.assessores','uses'=>'Admin\AssessoresController@index']);
Route::get('admin/assessores/adicionar',['as'=>'admin.assessores.adicionar','uses'=>'Admin\AssessoresController@adicionar']);
Route::post('admin/assessores/salvar',['as'=>'admin.assessores.salvar','uses'=>'Admin\AssessoresController@salvar']);
Route::get('admin/assessores/editar/{id}',['as'=>'admin.assessores.editar','uses'=>'Admin\AssessoresController@editar']);
Route::put('admin/assessores/atualizar/{id}',['as'=>'admin.assessores.atualizar','uses'=>'Admin\AssessoresController@atualizar']);
Route::get('admin/assessores/deletar/{id}',['as'=>'admin.assessores.deletar','uses'=>'Admin\AssessoresController@deletar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Eleitores */
Route::get('admin/eleitores',['as'=>'admin.eleitores','uses'=>'Admin\EleitoresController@index']);
Route::get('admin/eleitores/adicionar',['as'=>'admin.eleitores.adicionar','uses'=>'Admin\EleitoresController@adicionar']);

Route::post('admin/eleitores/salvar',['as'=>'admin.eleitores.salvar','uses'=>'Admin\EleitoresController@salvar']);
Route::get('admin/eleitores/editar/{id}',['as'=>'admin.eleitores.editar','uses'=>'Admin\EleitoresController@editar']);
Route::put('admin/eleitores/atualizar/{id}',['as'=>'admin.eleitores.atualizar','uses'=>'Admin\EleitoresController@atualizar']);
Route::get('admin/eleitores/deletar/{id}',['as'=>'admin.eleitores.deletar','uses'=>'Admin\EleitoresController@deletar']);
Route::get('admin/eleitores/consultar/{id}',['as'=>'admin.eleitores.consultar','uses'=>'Admin\EleitoresController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para o DashBoard */
Route::get('admin/dashboard',['as'=>'admin.dashboard','uses'=>'Admin\DashboardController@index']);
});


/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Visitas */
Route::get('admin/visitas',['as'=>'admin.visitas','uses'=>'Admin\VisitasController@index']);
Route::get('admin/visitas/adicionar',['as'=>'admin.visitas.adicionar','uses'=>'Admin\VisitasController@adicionar']);
Route::post('admin/visitas/salvar',['as'=>'admin.visitas.salvar','uses'=>'Admin\VisitasController@salvar']);

Route::get('admin/visitas/editar/{id}',['as'=>'admin.visitas.editar','uses'=>'Admin\VisitasController@editar']);
Route::get('admin/visitas/ata/{id}',['as'=>'admin.visitas.ata','uses'=>'Admin\VisitasController@ata']);
Route::get('admin/visitas/imprimir/{id}',['as'=>'admin.visitas.imprimir','uses'=>'Admin\VisitasController@imprimir']);

Route::put('admin/visitas/atualizar/{id}',['as'=>'admin.visitas.atualizar','uses'=>'Admin\VisitasController@atualizar']);
Route::get('admin/visitas/deletar/{id}',['as'=>'admin.visitas.deletar','uses'=>'Admin\VisitasController@deletar']);
Route::get('admin/visitas/consultar/{id}',['as'=>'admin.visitas.consultar','uses'=>'Admin\VisitasController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Categorias */
Route::get('admin/categorias',['as'=>'admin.categorias','uses'=>'Admin\categoriasController@index']);
Route::get('admin/categorias/adicionar',['as'=>'admin.categorias.adicionar','uses'=>'Admin\CategoriasController@adicionar']);
Route::post('admin/categorias/salvar',['as'=>'admin.categorias.salvar','uses'=>'Admin\CategoriasController@salvar']);
Route::get('admin/categorias/editar/{id}',['as'=>'admin.categorias.editar','uses'=>'Admin\CategoriasController@editar']);
Route::put('admin/categorias/atualizar/{id}',['as'=>'admin.categorias.atualizar','uses'=>'Admin\CategoriasController@atualizar']);
Route::get('admin/categorias/deletar/{id}',['as'=>'admin.categorias.deletar','uses'=>'Admin\CategoriasController@deletar']);
Route::get('admin/categorias/consultar/{id}',['as'=>'admin.categorias.consultar','uses'=>'Admin\CategoriasController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Categorias de cidadãos*/
Route::get('admin/catpessoas',['as'=>'admin.catpessoas','uses'=>'Admin\CatpessoasController@index']);
Route::get('admin/catpessoas/adicionar',['as'=>'admin.catpessoas.adicionar','uses'=>'Admin\CatpessoasController@adicionar']);
Route::post('admin/catpessoas/salvar',['as'=>'admin.catpessoas.salvar','uses'=>'Admin\CatpessoasController@salvar']);
Route::get('admin/catpessoas/editar/{id}',['as'=>'admin.catpessoas.editar','uses'=>'Admin\CatpessoasController@editar']);
Route::put('admin/catpessoas/atualizar/{id}',['as'=>'admin.catpessoas.atualizar','uses'=>'Admin\CatpessoasController@atualizar']);
Route::get('admin/catpessoas/deletar/{id}',['as'=>'admin.catpessoas.deletar','uses'=>'Admin\CatpessoasController@deletar']);
Route::get('admin/catpessoas/consultar/{id}',['as'=>'admin.catpessoas.consultar','uses'=>'Admin\CatpessoasController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Categorias de assessores*/
Route::get('admin/catassessores',['as'=>'admin.catassessores','uses'=>'Admin\CatassessoresController@index']);
Route::get('admin/catassessores/adicionar',['as'=>'admin.catassessores.adicionar','uses'=>'Admin\CatassessoresController@adicionar']);
Route::post('admin/catassessores/salvar',['as'=>'admin.catassessores.salvar','uses'=>'Admin\CatassessoresController@salvar']);
Route::get('admin/catassessores/editar/{id}',['as'=>'admin.catassessores.editar','uses'=>'Admin\CatassessoresController@editar']);
Route::put('admin/catassessores/atualizar/{id}',['as'=>'admin.catassessores.atualizar','uses'=>'Admin\CatassessoresController@atualizar']);
Route::get('admin/catassessores/deletar/{id}',['as'=>'admin.catassessores.deletar','uses'=>'Admin\CatassessoresController@deletar']);
Route::get('admin/catassessores/consultar/{id}',['as'=>'admin.catassessores.consultar','uses'=>'Admin\CatassessoresController@consultar']);
});


/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de SubCategorias */
Route::get('admin/subcategorias',['as'=>'admin.subcategorias','uses'=>'Admin\SubcategoriasController@index']);
Route::get('admin/subcategorias/adicionar',['as'=>'admin.subcategorias.adicionar','uses'=>'Admin\SubcategoriasController@adicionar']);
Route::post('admin/subcategorias/salvar',['as'=>'admin.subcategorias.salvar','uses'=>'Admin\SubcategoriasController@salvar']);
Route::get('admin/subcategorias/editar/{id}',['as'=>'admin.subcategorias.editar','uses'=>'Admin\SubcategoriasController@editar']);
Route::put('admin/subcategorias/atualizar/{id}',['as'=>'admin.subcategorias.atualizar','uses'=>'Admin\SubcategoriasController@atualizar']);
Route::get('admin/subcategorias/deletar/{id}',['as'=>'admin.subcategorias.deletar','uses'=>'Admin\SubcategoriasController@deletar']);
Route::get('admin/subcategorias/consultar/{id}',['as'=>'admin.subcategorias.consultar','uses'=>'Admin\SubcategoriasController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){

/* Rotas e métodos para a tabela de Demandas */
Route::get('admin/demandas',['as'=>'admin.demandas','uses'=>'Admin\DemandasController@index_abertas']);
Route::get('admin/demandas/fechadas',['as'=>'admin.demandas.fechadas','uses'=>'Admin\DemandasController@index_fechadas']);
Route::get('admin/demandas/logdemandas/{id}',['as'=>'admin.logdemandas','uses'=>'Admin\DemandasController@index_logdemandas']);

Route::get('admin/demandas/adicionar',['as'=>'admin.demandas.adicionar','uses'=>'Admin\DemandasController@adicionar']);
Route::get('admin/demandas/logadicionar/{id}',['as'=>'admin.demandas.logadicionar','uses'=>'Admin\DemandasController@logadicionar']);

Route::post('admin/demandas/salvar',['as'=>'admin.demandas.salvar','uses'=>'Admin\DemandasController@salvar']);
Route::post('admin/demandas/logsalvar',['as'=>'admin.demandas.logsalvar','uses'=>'Admin\DemandasController@logsalvar']);

Route::get('admin/demandas/editar/{id}',['as'=>'admin.demandas.editar','uses'=>'Admin\DemandasController@editar']);
Route::get('admin/demandas/fechar/{id}',['as'=>'admin.demandas.fechar','uses'=>'Admin\DemandasController@fechar']);
Route::get('admin/demandas/reabre/{id}',['as'=>'admin.demandas.reabre','uses'=>'Admin\DemandasController@reabre']);

//Route::get('admin/demandas/logadicionar',['as'=>'admin.logadicionar','uses'=>'Admin\DemandasController@logadicionar']);

Route::put('admin/demandas/atualizar/{id}',['as'=>'admin.demandas.atualizar','uses'=>'Admin\DemandasController@atualizar']);

Route::get('admin/demandas/deletar/{id}',['as'=>'admin.demandas.deletar','uses'=>'Admin\DemandasController@deletar']);

//Passa dois parametros o ID do registro principal e o ID do arquigo de Logs
Route::get('admin/demandas/logdeletar/{id}/{id1}',['as'=>'admin.demandas.logdeletar','uses'=>'Admin\DemandasController@logdeletar']);

Route::get('admin/demandas/consultar/{id}',['as'=>'admin.demandas.consultar','uses'=>'Admin\DemandasController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de Visitas */
Route::get('admin/compromissos',['as'=>'admin.compromissos','uses'=>'Admin\CompromissosController@index']);
Route::get('admin/compromissos/adicionar',['as'=>'admin.compromissos.adicionar','uses'=>'Admin\CompromissosController@adicionar']);
Route::post('admin/compromissos/salvar',['as'=>'admin.compromissos.salvar','uses'=>'Admin\CompromissosController@salvar']);
Route::get('admin/compromissos/editar/{id}',['as'=>'admin.compromissos.editar','uses'=>'Admin\CompromissosController@editar']);
Route::put('admin/compromissos/atualizar/{id}',['as'=>'admin.compromissos.atualizar','uses'=>'Admin\CompromissosController@atualizar']);
Route::get('admin/compromissos/deletar/{id}',['as'=>'admin.compromissos.deletar','uses'=>'Admin\CompromissosController@deletar']);
Route::get('admin/compromissos/consultar/{id}',['as'=>'admin.compromissos.consultar','uses'=>'Admin\CompromissosController@consultar']);

//retorna via ajax valores de imoveis
Route::post('retorna/subcategorias/',['as'=>'retorna_subcategorias','uses'=>'Admin\DemandasController@retorna_subcategorias']);

});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a tabela de usuarios */
Route::get('admin/usuarios',['as'=>'admin.usuarios','uses'=>'Admin\UsuariosController@index']);
Route::get('admin/usuarios/adicionar',['as'=>'admin.usuarios.adicionar','uses'=>'Admin\usuariosController@adicionar']);
Route::post('admin/usuarios/salvar',['as'=>'admin.usuarios.salvar','uses'=>'Admin\UsuariosController@salvar']);
Route::get('admin/usuarios/editar/{id}',['as'=>'admin.usuarios.editar','uses'=>'Admin\UsuariosController@editar']);
Route::put('admin/usuarios/atualizar/{id}',['as'=>'admin.usuarios.atualizar','uses'=>'Admin\UsuariosController@atualizar']);
Route::get('admin/usuarios/deletar/{id}',['as'=>'admin.usuarios.deletar','uses'=>'Admin\UsuariosController@deletar']);
Route::get('admin/usuarios/consultar/{id}',['as'=>'admin.usuarios.consultar','uses'=>'Admin\UsuariosController@consultar']);
});

/*Protege os dados coma middleware */
Route::group(['middleware'=>'auth'], function(){
/* Rotas e métodos para a FullCalendar */
Route::get('compromissos/get',['as'=>'calendario','uses'=>'Site\HomeController@get_events']);
});
