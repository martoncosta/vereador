@extends('layout.site')

@section('titulo','Redefinir Senha')

@section('conteudo')

  <div class="login center">

    <h4>Redefir Senha</h4>

    <div class="row col s4">

       <form class="login form"  role="form" method="POST" action="{{ url('/password/email') }}">

            {{ csrf_field() }}

            <div class="input-field col s4 offset-s4 card">

             <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

                <label for="email" class="col s4 control-label"><i class="material-icons left">email</i>Email</label>

                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                <div class="col s4">

                  @if ($errors->has('email'))
                    <strong>{{ $errors->first('email') }}</strong>
                    <span class="help-block">
                    </span>
                  @endif

                </div>

             </div>

           </div>

           <br/>

          <div class="input-field col s4 offset-s4">
                  <button type="submit" class="btn btn-primary">
                      Enviar email com link para redefinir senha
                  </button>
          </div>

        </form>

        </div>

   </div>

@endsection
