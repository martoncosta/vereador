@extends('layout.site')

@section('titulo','Redefinir Senha')

@section('conteudo')

<div class="login center">

    <h4>Redefir Senha</h4>

    <div class="row col s4">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                      <div class="input-field col s4 offset-s4 card">

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                                <label for="email" class="col-md-4 control-label"><i class="material-icons left">email</i>Informe email</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>

                    <div class="input-field col s4 offset-s4 card">

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                        </div>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                                <label for="password" class="col-md-4 control-label"><i class="material-icons left">vpn_key</i>Senha</label>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                @endif
                            </div>
                    </div>

                    <div class="input-field col s4 offset-s4 card">

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                            <div class="col-md-6">

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                <label for="password-confirm" class="col-md-4 control-label"><i class="material-icons left">vpn_key</i>Confirma Senha</label>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif

                            </div>
                       </div>

                     </div>

                    <div class="input-field col s4 offset-s4">

                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Redefine Senha
                                </button>
                            </div>

                    </div>


                    </form>
                  </div>

</div>

@endsection
