@extends('layout.site')

@section('titulo','login')

@section('conteudo')

  <div class="login center">

    <h3 class="center"></h3>
    <div class="row">

      <form class="login form" action="{{route('site.login.entrar')}}" method="post">

        {{ csrf_field() }}

        <div class="row" {{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="input-field col s4 offset-s4 card">
              <input type="text" name="email">
              <label><i class="material-icons left">email</i>E-mail</label>

              @if ($errors->has('email'))
                     <span class="help-block">
                         <strong>{{ $errors->first('email') }}</strong>
                     </span>
              @endif

            </div>
        </div>

        <div class="row">
            <div class="input-field col s4 offset-s4 card">
              <input type="password" name="senha">
              <label><i class="material-icons left">vpn_key</i>Senha</label>

              @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
              @endif

            </div>
        </div>

        <button class="btn deep-green  center">Login</button>

      <div class="row">
          <br/>
          <a class="recuperar" href="{{ url('/password/reset') }}">Recuperar Senha?</a>
      </div>

   </form>

  </div>
</div>

@endsection
