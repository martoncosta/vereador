@extends('layout.site')

@section('titulo','Visitas')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar Categoria</h3>
    <div class="row">
      <form class="" action="{{route('admin.categorias.salvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.categorias._form')

            <div class="row">
                <div class="col sm-2">
                  <button type="submit" class="btn deep-blue">Salvar</button>
                </div>

                <div class="col sm-2">
                  <a class="btn red"  href="{{route('admin.categorias')}}">Cancelar</a>'
                </div>
            </div>
        </form>
    </div>
  </div>

@endsection
