@extends('layout.site')

@section('titulo','Compromissos')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar Compromissos</h3>
    <div class="row">
      <form class="" action="{{route('admin.compromissos.salvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.compromissos._form')
        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.compromissos')}}">Cancelar</a>'
            </div>
        </div>
      </form>
    </div>
  </div>

@endsection
