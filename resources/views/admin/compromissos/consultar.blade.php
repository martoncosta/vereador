@extends('layout.site')

@section('titulo','Compromissos')

@section('conteudo')
  <div class="container">
    <h3 class="center">Consultando Compromissos</h3>
    <div class="row">

      <form class="" action="{{route('admin.compromissos.atualizar',$registro->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.compromissos._form_consulta')

        <div class="col sm-2">
            <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
        </div>

        <div class="row">
            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.compromissos')}}">Retornar</a>'
            </div>
        </div>
      </form>

    </div>
  </div>

@endsection
