@extends('layout.site')

@section('titulo','Compromissos')

@section('conteudo')

<div class="container" align="center">

    <h5><i class="material-icons left">business_center</i>Compromissos</h5>

    <div class="input-field col s12">
      <i class="material-icons prefix">search</i>
      <input id="txt_consulta" type="tel" class="validate">
      <label for="txt_consulta">Digite o texto para pesquisar</label>
    </div>

    <div class="row">

      <a class = "btn deep-blue" href="{{ route('admin.compromissos.adicionar')}}"><i class="material-icons left">add_circle</i>Adicionar</a>

    </div>

    <div class="row">
      <a> Total de compromissos : {{ $qtdregistros }}</a>
    </div>

  <div class="row">
      <table id="tabela" class="responsive-table striped bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Compromisso</th>
            <th>Descrição</th>
            <th>Data Inicial / Hora</th>
            <th>Data Final / Hora</th>
            <th> A ç õ e s</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)
            <tr>
              <td>{{ $registro->id}}</td>
              <td>{{ $registro->title}}</td>
              <td>{{ $registro->descricao}}</td>
              <td>{{ date( 'd/m/Y H:i' , strtotime($registro->start))}} </td>
              <td>{{ date( 'd/m/Y H:i' , strtotime($registro->end))}} </td>

              <td>
                <a class="btn-small" href="{{ route('admin.compromissos.editar', $registro->id)}}">Editar   </a>
                <a class="btn-small" href="{{ route('admin.compromissos.consultar', $registro->id)}}">Visualiza</a>
                <a class="btn-small" href="{{ route('admin.compromissos.deletar', $registro->id)}}">Apagar</a>
              </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

    {{ $registros->links() }}

  </div>

</div>

@endsection
