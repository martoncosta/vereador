
<div class="row">

  <div class="input-field col s10">
    <input type="text" style="color: black" name="title" value="{{isset($registro->title) ? $registro->title : ''}}" required disabled>
    <label>Titulo do compromisso'</label>
  </div>

</div>

<div class="row">

  <div class="input-field col s10">
    <input type="text" style="color: black" name="descricao" value="{{isset($registro->descricao) ? $registro->descricao : ''}}" required disabled>
    <label>Descrição'</label>
  </div>

</div>

<div class="row">
        <div class="input-field col s3">
        <input type="text" style="color: black" class="datepicker" id="datapickerstart" name="start" value="{{isset($registro->start) ? date( 'd/m/Y H:i' , strtotime($registro->start)) : ''}}" required disabled>
        <label>Data Inicial</label>
      </div>
</div>

<div class="row">
        <div class="input-field col s3">
        <input type="text" style="color: black" class="datepicker" id="datapickerend" name="end" value="{{isset($registro->end) ? date( 'd/m/Y H:i' , strtotime($registro->end)) : ''}}" required disabled>
        <label>Data Final</label>
      </div>
</div>
