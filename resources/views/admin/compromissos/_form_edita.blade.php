
<div class="row">

  <div class="input-field col s10">
    <input type="text" name="title" value="{{isset($registro->title) ? $registro->title : ''}}" required>
    <label>Titulo do compromisso'</label>
  </div>

</div>

<div class="row">

  <div class="input-field col s10">
    <input type="text" name="descricao" value="{{isset($registro->descricao) ? $registro->descricao : ''}}" required>
    <label>Descrição'</label>
  </div>

</div>



<div class="row">

    <div class="col s4">
      <label for="start">Data Inicial</label>
      <input type="datetime" class="datepicker" id="start" name="start" value="{{isset($registro->start) ? date('d-m-Y H:i', strtotime($registro->start)) : ''}}">
    </div>

</div>


<div class="row">

    <div class="col s4">

      <label for="end">Data Final</label>
      <input type="datetime" class="datapicker" id="end" name="end" value="{{isset($registro->end) ? date('d-m-Y H:i', strtotime($registro->end)) : ''}}" required>

    </div>

</div>
