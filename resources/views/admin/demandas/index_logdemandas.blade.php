@extends('layout.site')

@section('titulo','Demandas')

@section('conteudo')

<div class="container" align="center">
  <h4><i class="material-icons left">assignment</i>Histórico demandas</h4>

  <div class="input-field col s12">
    <i class="material-icons prefix">search</i>
    <input id="txt_consulta" type="tel" class="validate">
    <label for="txt_consulta">Digite o texto para pesquisar</label>
  </div>

    <div class="row">
      <a class = "btn deep-blue" href="{{ route('admin.demandas.logadicionar', $iddemand) }}"><i class="material-icons left">add_circle</i>Adicionar histórico</a>
      <a class = "btn red" href="{{ route('admin.demandas')}}"><i class="material-icons left">arrow_back</i>Retornar</a>
    </div>

    <div class="row">
      <a> Total de históricos : {{ $qtdregistros }}</a>
    </div>

  <div class="row">

    <table id="tabela" class="responsive-table striped bordered" >
        <thead>
          <tr>
            <th>id</th>
            <th>Data</th>
            <th>Historico</th>
            <th> Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)
            <tr>
              <td>{{ $registro->id }}
              <td>{{ $registro->created_at }}
              <td>{{ $registro->logdescricao }}

              <td>
                <a class="btn-small" href="{{ route('admin.demandas.logdeletar', [ 'k'=>$registro->id, 'l'=>$iddemand ])}}">Excluir</a>
              </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

  </div>

</div>

@endsection
