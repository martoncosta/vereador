@extends('layout.site')

@section('titulo','Demandas Fechar')

@section('conteudo')
  <div class="container">
    <h3 class="center">Fechando demanda</h3>
    <div class="row">

      <form class="" action="{{route('admin.demandas.atualizar',$registros->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.demandas._form_fecha')

        <div class="row">
        		<div class="col sm-2">
        			<button type="submit" class="btn deep-blue">Salvar</button>
        		</div>

            <div class="col sm-2">

              <a class="btn red"  href="{{route('admin.demandas')}}">Cancelar</a>

        		</div>
        </div>

      </form>


    </div>
  </div>

@endsection
