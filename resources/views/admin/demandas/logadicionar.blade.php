@extends('layout.site')

@section('titulo','Historico demandas')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar Histórico demandas</h3>
    <div class="row">
      <form class="" action="{{route('admin.demandas.logsalvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        @include('admin.demandas._form_logdemandas')

        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.logdemandas', $iddemands)}}">Cancelar</a>'
            </div>
        </div>

      </form>
    </div>
  </div>

@endsection
