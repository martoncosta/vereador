@extends('layout.site')

@section('titulo','Demandas')

@section('conteudo')

<div class="container" align="center">
  <h5><i class="material-icons left">business_center</i>Demandas abertas</h5>

    <div class="input-field col s12">
      <i class="material-icons prefix">search</i>
      <input id="txt_consulta" type="tel" class="validate">
      <label for="txt_consulta">Digite o texto para pesquisar</label>
    </div>

    <div class="row">
      <a class = "btn deep-blue" href="{{ route('admin.demandas.adicionar')}}"><i class="material-icons left">add_circle</i>Adicionar</a>
    </div>

    <div class="row">
      <a> Total de demandas : {{ $qtdregistros }}</a>
    </div>

  <div class="row">

    <table id="tabela" class="responsive-table striped bordered" >
        <thead>
          <tr>
            <th>Dias Aberto</th>
            <th>Status</th>
            <th>Código</th>
            <th>Abertura</th>
            <th>Categoria</th>
            <th>Subcategoria</th>
            <th>Endereco</th>
            <th>Descricao</th>
            <th>Cidadão</th>
            <th>Assessor</th>
            <th> Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)
            <tr>
              <td>{{ $registro->prazo }}
              <td>{{ $registro->status }}
              <td>{{ $registro->id }}
              <td>{{ date( 'd/m/Y H:i' , strtotime($registro->databertura))}} </td>
              <td>{{ $registro->descricaocategoria }}
              <td>{{ $registro->subcategoria }}
              <td>{{ $registro->endereco }}
              <td>{{ $registro->descricao }}
              <td>{{ $registro->nome }}
              <td>{{ $registro->nomeassessor }}

              <td>
                <a class="btn-small" href="{{ route('admin.demandas.editar', $registro->id)}}">Editar  </a>
                <a class="btn-small" href="{{ route('admin.logdemandas', $registro->id)}}">Memo</a>
                <a class="btn-small" href="{{ route('admin.demandas.fechar', $registro->id)}}">Fechar</a>
              </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

  </div>

</div>

@endsection
