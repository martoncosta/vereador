@extends('layout.site')

@section('titulo','Demandas')

@section('conteudo')

<div class="container" align="center">
  <h5><i class="material-icons left">business_center</i>Demandas Fechadas</h5>

  <div class="input-field col s12">
    <i class="material-icons prefix">search</i>
    <input id="txt_consulta" type="tel" class="validate">
    <label for="txt_consulta">Digite o texto para pesquisar</label>
  </div>

    <div class="row">
      <a> Total de demandas : {{ $qtdregistros }}</a>
    </div>

  <div class="row">

    <table id="tabela" class="responsive-table striped bordered" >
        <thead>
          <tr>
            <th>Código</th>
            <th>Abertura</th>
            <th>Categoria</th>
            <th>Subcategoria</th>
            <th>Endereço</th>
            <th>Descricao</th>
            <th>Fechado</th>
            <th>solucao</th>
            <th>Cidadão</th>
            <th>Assessor</th>
            <th> Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)
            <tr>
              <td>{{ $registro->id }}
              <td>{{ date( 'd/m/Y H:i' , strtotime($registro->databertura))}}</td>
              <td>{{ $registro->descricaocategoria }}
              <td>{{ $registro->subcategoria }}
              <td>{{ $registro->endereco }}
              <td>{{ $registro->descricao }}
              <td>{{ date( 'd/m/Y H:i' , strtotime($registro->datafechamento))}}</td>
              <td>{{ $registro->solucao }}
              <td>{{ $registro->nome }}
              <td>{{ $registro->nomeassessor }}

              <td>
                <a class="btn-small" href="{{ route('admin.demandas.reabre', $registro->id)}}">Reabre  </a>
                <a class="btn-small" href="{{ route('admin.demandas.consultar', $registro->id)}}">Visualiza</a>
              </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

  </div>

</div>

@endsection
