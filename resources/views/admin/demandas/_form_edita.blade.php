
<div class="row">

  <div class="col s4">

      <label for="categoria1"> Informe a categoria </label>

            <select name="categoria" id="categoria1" class="form-control" required>

              <option value="{{ $registros->categoria }}" selected>{{isset($registros->categoria) ? $regcategorias[$registros->categoria-1]->descricaocategoria : ''}}</option>

              @if(!isset($registros->categoria))
                @foreach( $regcategorias as $regcategoria )
                  <option value="{{$regcategoria->id}}">{{$regcategoria->descricaocategoria}}</option>
                @endforeach
              @endif

           </select>

  </div>

  <div class="col s4">

      <label for="subcategoria1"> Subcategoria </label>

      <select name="subcategoria" id="subcategoria1" class="form-control" required>

        <option value="{{ $registros->subcategoria }}" selected>{{isset($registros->subcategoria) ? $regsubcategorias[$registros->subcategoria-1]->subcategoria : ''}}</option>

        @if(!isset($registros->subcategoria))
          @foreach( $regsubcategorias as $regsubcategoria )
            <option value="{{$regsubcategoria->id}}">{{$regsubcategoria->subcategoria}}</option>
          @endforeach
        @endif

     </select>

  </div>

  <div class="col s3">
    <label for="prazo">Prazo</label>
    <input type="text" name="prazo" id="prazo" value="{{isset($registros->prazo) ? $registros->prazo : ''}}"/>
  </div>

</div>

<div class="row">

  <div class="input-field col s5">
    <label for="endereco">Endereco</label>
    <input type="text" name="endereco" id="endereco" value="{{isset($registros->endereco) ? $registros->endereco : ''}}" required>
  </div>

  <div class="input-field col s3">
    <label for="bairro">Bairro</label>
    <input type="text" name="bairro" id="bairro" value="{{isset($registros->bairro) ? $registros->bairro : ''}}" required>
  </div>

</div>

<div class="row">

  <div class="col s11">

      <textarea placeholder="Descreva a demanda" rows="6", cols="54" id="descricao" name="descricao" style="resize:vertical" >{{isset($registros->descricao) ? $registros->descricao : ''}}</textarea>

   </div>

 </div>

<div class="row">

  <div class="col s5">

      <label for="eleitor1"> Informe o Eleitor </label>

      <select name="eleitor" id="eleitor1" class="browser-default" required>

        <option value="{{$registros->eleitor}}" selected>{{isset($registros->eleitor) ? $regeleitores[$registros->eleitor-1]->nome : ''}}</option>

        @foreach( $regeleitores as $eleitor )
          <option value="{{$eleitor->id}}">{{$eleitor->nome}}</option>
        @endforeach

     </select>
   </div>

   <div class="col s5">

       <label for="assessor1"> Informe o Assessor </label>

       <select name="assessor" id="assessor1" class="browser-default" required>

         <option value="{{$registros->assessor}}" selected>{{isset($registros->assessor) ? $regassessores[$registros->assessor-1]->nomeassessor : ''}}</option>

         @foreach( $regassessores as $assessor )
           <option value="{{$assessor->id}}">{{$assessor->nomeassessor}}</option>
         @endforeach

      </select>
    </div>

    <div class="">

      <input type="hidden" name="status"         value="{{isset($registros->status)         ? $registros->status :         'aberto'}}"/>
      <input type="hidden" name="datafechamento" value="{{isset($registros->datafechamento) ? $registros->datafechamento : ''}}"/>
      <input type="hidden" name="databertura"    value="{{isset($registros->databertura) ? $registros->databertura : ''}}"/>

    </div>


</div>
