
<div class="row">

  <div class="col s4">

      <label for="categoria1"> Informe a categoria </label>

      <select name="categoria" id="categoria" class="form-control m-bot15" required>

        <option value="{{ isset($registros->categors_id) }}" selected>{{isset($registros->categors_id) ? $regcategorias[$registros->categors_id-1]->descricaocategoria : ''}}</option>

        @if(!isset($registros->categors_id))
          @foreach( $regcategorias as $regcategoria )
            <option value="{{$regcategoria->id}}">{{$regcategoria->descricaocategoria}}</option>
          @endforeach
        @endif

     </select>

  </div>

  <div class="col s4">

      <label for="subcategoria"> Informe a Subcategoria </label>

      <select  name="subcategoria" id="myDropdown" class="materialSelect">
      </select>

  </div>

    {{--<div class="col s2">--}}
        {{--<button class="waves-effect waves-light btn" id="myButton">--}}
            {{--Add New Option to Dropdown--}}
        {{--</button>--}}


    {{--</div>--}}

  <div class="col s2">
    <label for="prazo">Prazo</label>
    <input type="text" name="prazo" id="prazo" value="{{isset($registro->prazo) ? $registro->prazo : ''}}" required/>
  </div>

</div>

<div class="row">

  <div class="input-field col s5">
    <label for="endereco">Endereco</label>
    <input type="text" name="endereco" id="endereco" value="{{isset($registro->endereco) ? $registro->endereco : ''}}" required>
  </div>

  <div class="input-field col s3">
    <label for="bairro">bairro</label>
    <input type="text" name="bairro" id="bairro" value="{{isset($registro->bairro) ? $registro->bairro : ''}}" required>
  </div>

</div>

<div class="row">

  <div class="col s11">

      <textarea placeholder="Descreva a demanda" rows="6", cols="54" id="descricao" name="descricao" style="resize:vertical" >{{isset($registro->descricao) ? $registro->descricao : ''}}</textarea>

   </div>

</div>

<div class="row">
  <div class="col s4">

      <label for="eleitor1"> Informe o Eleitor </label>

      <select name="eleitor" id="eleitor1" class="browser-default" required>

        <option value="{{isset($registro->eleitors_id)}}" selected>{{isset($registro->eleitors_id) ? $regeleitores[$registro->eleitors-1]->nome : ''}}</option>

        @foreach( $regeleitores as $eleitor )
          <option value="{{$eleitor->id}}">{{$eleitor->nome}}</option>
        @endforeach

     </select>
   </div>

   <div class="col s4">

       <label for="assessor1"> Informe o Assessor </label>

       <select name="assessor" id="assessor1" class="browser-default" required>

         <option value="{{isset($registro->assessors_id)}}" selected>{{isset($registro->nomeassessor) ? $regassessores[$registro->assessor-1]->nome : ''}}</option>

         @foreach( $regassessores as $assessor )
           <option value="{{$assessor->id}}">{{$assessor->nomeassessor}}</option>
         @endforeach

      </select>
    </div>

    <div class="">

      <input type="hidden" name="status"         value="{{isset($registro->status)         ? $registro->status :         'aberto'}}"/>
      <input type="hidden" name="datafechamento" value="{{isset($registro->datafechamento) ? $registro->datafechamento : '2018-01-01'}}"/>
      <input type="hidden" name="databertura"  id="dtabertura" />

    </div>

    <meta name="csrf-token" content="{{ csrf_token() }}">


</div>
