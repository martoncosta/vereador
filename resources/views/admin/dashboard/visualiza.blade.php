@extends('layout.site')

@section('titulo','Dashboard')

@section('conteudo')

<div class="container" align="center">

    <h5><i class="material-icons left">business_center</i>DashBoard</h5>

    <div class="row" >
       <table class="striped">
          <thead>
            <tr>
                <th>Indicador</th>
                <th>Registros</th>
                <th>Jan</th>
                <th>Fev</th>
                <th>Mar</th>
                <th>Abr</th>
                <th>Mai</th>
                <th>Jul</th>
                <th>Jul</th>
                <th>Ago</th>
                <th>Set</th>
                <th>Out</th>
                <th>Nov</th>
                <th>Dez</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Assessores</td>
              <td>{{ $regassessores}}</td><td>{{ $regassessorjan}}</td></td><td>{{ $regassessorfev}}</td><td>{{ $regassessormar}}</td><td>{{ $regassessorabr}}</td><td>{{ $regassessormai}}</td><td>{{ $regassessorjun}}</td><td>{{ $regassessorjul}}</td><td>{{ $regassessorago}}</td><td>{{ $regassessorset}}</td><td>{{ $regassessorout}}</td><td>{{ $regassessornov}}</td><td>{{ $regassessordez}}</td>
            </tr>
            <tr>
              <td>Eleitores</td>
              <td>{{ $regeleitores}}<td>{{ $regeleitorjan}}</td></td><td>{{ $regeleitorfev}}</td><td>{{ $regeleitormar}}</td><td>{{ $regeleitorabr}}</td><td>{{ $regeleitormai}}</td><td>{{ $regeleitorjun}}</td><td>{{ $regeleitorjul}}</td><td>{{ $regeleitorago}}</td><td>{{ $regeleitorset}}</td><td>{{ $regeleitorout}}</td><td>{{ $regeleitornov}}</td><td>{{ $regeleitordez}}</td>
            </tr>

            <tr><td>Demandas Abertas</td> <td> Demanas Fechadas </td>
            <tr>
              <td>{{ $regdemandasabertas}}</td><td>{{ $regdemandasfechadas}}</td><td>{{ $regdemandasjan}}</td><td>{{ $regdemandasfev}}</td><td>{{ $regdemandasmar}}</td><td>{{ $regdemandasabr}}</td><td>{{ $regdemandasmai}}</td><td>{{ $regdemandasjun}}</td><td>{{ $regdemandasjul}}</td><td>{{ $regdemandasago}}</td><td>{{ $regdemandasset}}</td><td>{{ $regdemandasout}}</td><td>{{ $regdemandasnov}}</td><td>{{ $regdemandasdez}}</td>
            </tr>

            <tr>
              <td>Visitas</td>
              <td>{{ $regvisitas}}</td><td>{{ $regvisitasjan}}</td><td>{{ $regvisitasfev}}</td><td>{{ $regvisitasmar}}</td><td>{{ $regvisitasabr}}</td><td>{{ $regvisitasmai}}</td><td>{{ $regvisitasjun}}</td><td>{{ $regvisitasjul}}</td><td>{{ $regvisitasago}}</td><td>{{ $regvisitasset}}</td><td>{{ $regvisitasout}}</td><td>{{ $regvisitasnov}}</td><td>{{ $regvisitasdez}}</td>
            </tr>
            <tr>
              <td>Compromissos</td>
              <td>{{ $regcompromissos}}</td><td>{{ $regcomprojan}}</td><td>{{ $regcomprofev}}</td><td>{{ $regcompromar}}</td><td>{{ $regcomproabr}}</td><td>{{ $regcompromai}}</td><td>{{ $regcomprojun}}</td><td>{{ $regcomprojul}}</td><td>{{ $regcomproago}}</td><td>{{ $regcomproset}}</td><td>{{ $regcomproout}}</td><td>{{ $regcompronov}}</td><td>{{ $regcomprodez}}</td>
            </tr>


          </tbody>

          <div class="col sm-2">
              <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
          </div>
          <div class="col sm-2">
            <a class="btn red"  href="{{route('site.home')}}">Retornar</a>
          </div>

  </div>

</div>

@endsection
