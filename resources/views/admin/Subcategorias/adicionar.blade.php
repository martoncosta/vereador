@extends('layout.site')

@section('titulo','Subcategorias')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar Subcategoria</h3>
    <div class="row">
      <form class="" action="{{route('admin.subcategorias.salvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.subcategorias._form')
        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.subcategorias')}}">Cancelar</a>'
            </div>
        </div>
      </form>
    </div>
  </div>

@endsection
