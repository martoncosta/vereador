
<div class="row">

  <div class="input-field col s4">

      <select name="categoria_id" id="categoria" class="form-control" required>

        <option value="{{ isset($registros->categoria_id) }}" selected>{{isset($registros->categoria_id) ? $regcategorias[$registros->categoria_id-1]->descricao : ''}}</option>

        @if(!isset($registros->categoria_id))
          @foreach( $regcategorias as $regcategoria )
            <option value="{{$regcategoria->id}}">{{$regcategoria->descricaocategoria}}</option>
          @endforeach
        @endif

     </select>

  </div>

  <div class="input-field col s10">
    <input type="text" name="subcategoria" value="{{isset($registros->subcategoria) ? $registros->subcategoria : ''}}">
    <label>Descreva Subcategoria</label>
  </div>


</div>
