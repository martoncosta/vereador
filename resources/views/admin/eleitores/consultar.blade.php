@extends('layout.site')

@section('titulo','Eleitores')

@section('conteudo')
  <div class="container">
    <h3 class="center">Consultando Eleitores</h3>
    <div class="row">
      <form class="" action=" {{route('admin.eleitores')}}" enctype="multipart/form-data">

        {{ csrf_field() }}
        @include('admin.eleitores._form_consulta')

        <div class="col sm-2">
            <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
        </div>

        <button class="btn red">Retornar</button>

      </form>

  </div>

@endsection
