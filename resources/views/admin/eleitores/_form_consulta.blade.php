
<div class="row">

  <div class="input-field col s3">
    <input type="text" style="color: black" name="nome" value="{{isset($registro->nome) ? $registro->nome : ''}}" disabled>
    <label>Nome</label>
  </div>

  <div class="input-field col s3">
    <input type="text" style="color: black" name="endereco" id="endereco" value="{{isset($registro->endereco) ? $registro->endereco : ''}}" disabled>
    <label>Endereco</label>
  </div>

  <div class="input-field col s3">

      <select name="assessor" id="assessor" class="form-control" required disabled>

        <option value="{{$registro->assessor}}" selected>{{isset($registro->assessor) ? $regassessores[$registro->assessor-1]->nomeassessor : ''}}</option>

        @foreach( $regassessores as $assessor )
          <option value="{{$assessor->id}}">{{$assessor->nomeassessor}}</option>
        @endforeach

     </select>

  </div>

  <div class="input-field col s3">

      <select name="tipopessoa" id="tipopessoa" class="form-control" required disabled>

        <option value="{{$registro->tipopessoa}}" selected>{{isset($registro->tipopessoa) ? $regtipopessoa[$registro->tipopessoa-1]->descatpessoa: ''}}</option>

        @foreach( $regtipopessoa as $tipopessoa )
          <option value="{{$tipopessoa->id}}">{{$tipopessoa->descatpessoa}}</option>
        @endforeach

     </select>

  </div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" style="color: black" name="bairro" id="bairro" value="{{isset($registro->bairro) ? $registro->bairro : ''}}" disabled>
      <label>Bairro</label>
    </div>

    <div class="input-field col s3">
      <input type="text" style="color: black" name="cidade" id="cidade" value="{{isset($registro->cidade) ? $registro->cidade : ''}}" disabled>
      <label>Cidade</label>
    </div>

    <div class="input-field col s3">
      <input type="text" style="color: black" name="estado" id="estado" value="{{isset($registro->estado) ? $registro->estado : ''}}" disabled>
      <label>UF</label>
    </div>

  <div class="input-field col s2">
      <input type="text" style="color: black" data-lenght="9" name="cep" id="cep" value="{{isset($registro->cep) ? $registro->cep : ''}}" disabled>
      <label>Cep</label>
    </div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" style="color: black" class="datepicker" name="dtnascimento" id="dtnascimento" value="{{isset($registro->dtnascimento) ? $registro->dtnascimento : ''}}" disabled>
      <label>Data Nascimento</label>
    </div>

    <div class="input-field col s3">
      <input type="text" style="color: black" name="identidade" id="identidade" value="{{isset($registro->identidade) ? $registro->identidade : ''}}" disabled>
      <label>Identidade</label>
    </div>

    <div class="input-field col s3">
      <input type="text" style="color: black" name="cpf" id="cpf" value="{{isset($registro->cpf) ? $registro->cpf : ''}}" disabled>
      <label>CPF</label>
    </div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" style="color: black" name="telefone" id="telefoneelei" value="{{isset($registro->telefone) ? $registro->telefone : ''}}" disabled>
      <label>Telefone</label>
    </div>

    <div class="input-field col s3">
      <input type="text" style="color: black" name="celular" id="celularelei" value="{{isset($registro->celular) ? $registro->celular : ''}}" disabled>
      <label>Celular</label>
    </div>

    <div class="input-field col s4">
      <input type="email" style="color: black" name="email" value="{{isset($registro->email) ? $registro->email : ''}}" disabled>
      <label>email</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
      <input type="text" style="color: black" name="observacoes" value="{{isset($registro->observacoes) ? $registro->observacoes : ''}}" disabled>
      <label>observacoes</label>
    </div>
</div>
