@extends('layout.site')

@section('titulo','Eleitores')

@section('conteudo')
  <div class="container">
    <h3 class="center">Editando Eleitores</h3>
    <div class="row">
      <form class="" action="{{route('admin.eleitores.atualizar',$registro->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.eleitores._form_edita')
        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.eleitores')}}">Cancelar</a>'
            </div>
        </div>
      </form>
    </div>
  </div>

@endsection
