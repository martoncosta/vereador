<div class="row">

  <div class="input-field col s3">
    <input type="text" name="nome" value="{{isset($registro->nome) ? $registro->nome : ''}}">
    <label>Nome</label>
  </div>

  <div class="input-field col s3">
    <input type="text" name="endereco" id="endereco" value="{{isset($registro->endereco) ? $registro->endereco : ''}}">
    <label>Endereco</label>
  </div>

  <div class="col s3">

      <label for="assessor1"> Informe o Assessor </label>

      <select name="assessor" id="assessor" class="browser-default" required placeholder="Selecione Assessor">

        <option value="{{isset($registro->assessor)}}" selected>{{isset($registro->assessor) ? $regassessores[$registro->assessor-1]->nomeassessor: ''}}</option>

        @foreach( $regassessores as $assessor )
          <option value="{{$assessor->id}}">{{$assessor->nomeassessor}}</option>
        @endforeach

     </select>

  </div>


    <div class="col s3">

        <label for="tipopessoa"> Informe Tipo Pessoa </label>

        <select name="tipopessoa" id="tipopessoa" class="browser-default" required>

          <option value="{{isset($registro->tipopessoa)}}" selected>{{isset($registro->tipopessoa) ? $regtipopessoa[$registro->tipopessoa-1]->descatpessoa: ''}}</option>

          @foreach( $regtipopessoa as $tipopessoa )
            <option value="{{$tipopessoa->id}}">{{$tipopessoa->descatpessoa}}</option>
          @endforeach

       </select>

    </div>



</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" name="bairro" id="bairro" value="{{isset($registro->bairro) ? $registro->bairro : ''}}">
      <label>Bairro</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="cidade" id="cidade" value="{{isset($registro->cidade) ? $registro->cidade : ''}}">
      <label>Cidade</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="estado" id="estado" value="{{isset($registro->estado) ? $registro->estado : ''}}">
      <label>UF</label>
    </div>

  <div class="input-field col s2">
      <input type="text" data-lenght="9" name="cep" id="cep" value="{{isset($registro->cep) ? $registro->cep : ''}}">
      <label>Cep</label>
    </div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" class="datepicker" name="dtnascimento" id="dtnascimento" value="{{isset($registro->dtnascimento) ? $registro->dtnascimento : ''}}">
      <label>Data Nascimento</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="identidade" id="identidade" value="{{isset($registro->identidade) ? $registro->identidade : ''}}">
      <label>Identidade</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="cpf" id="cpf" value="{{isset($registro->cpf) ? $registro->cpf : ''}}">
      <label>CPF</label>
    </div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" name="telefone" id="telefoneelei" value="{{isset($registro->telefone) ? $registro->telefone : ''}}">
      <label>Telefone</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="celular" id="celularelei" value="{{isset($registro->celular) ? $registro->celular : ''}}">
      <label>Celular</label>
    </div>

    <div class="input-field col s4">
      <input type="email" name="email" value="{{isset($registro->email) ? $registro->email : ''}}">
      <label>email</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
      <input type="text" name="observacoes" value="{{isset($registro->observacoes) ? $registro->observacoes : ''}}">
      <label>observacoes</label>
    </div>
</div>
