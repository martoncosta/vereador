@extends('layout.site')

@section('titulo','Eleitores')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar Eleitores</h3>
    <div class="row">
      <form class="" action="{{route('admin.eleitores.salvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.eleitores._form')
        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.eleitores')}}">Cancelar</a>'
            </div>
        </div>
      </form>
    </div>
  </div>

@endsection
