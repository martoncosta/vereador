@extends('layout.site')

@section('titulo','Eleitores')

@section('conteudo')

<div class="container" align="center">

  <h5><i class="material-icons left">face</i>Eleitores</h5>

  <div class="input-field col s12">
    <i class="material-icons prefix">search</i>
    <input id="txt_consulta" type="tel" class="validate">
    <label for="txt_consulta">Digite o texto para pesquisar</label>
  </div>

  <div class="row">
    <a class = "btn deep-blue" href="{{ route('admin.eleitores.adicionar')}}"><i class="material-icons left">add_circle</i>Adicionar</a>
  </div>

  <div class="row">
    <a> Total de eleitores : {{ $qtdregistros }}</a>
  </div>

  <div class="row">
      <table id="tabela" class="responsive-table striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Tp cidadão</th>
            <th>Assessor</th>
            <th>Nome</th>
            <th>Endereco</th>
            <th>Cidade</th>
            <th>bairro</th>
            <th>UF</th>
            <th>Cep</th>
            <th>Celular</th>
            <th>A ç o e s</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)

            <tr>
              <td>{{ $registro->id}}</td>

              <td>{{ $regtipopessoa[$registro->tipopessoa-1]->descatpessoa}}</td>
              <td>{{ $regassessores[$registro->assessor-1]->nomeassessor}}</td>

              <td>{{ $registro->nome}}</td>
              <td>{{ $registro->endereco}}</td>
              <td>{{ $registro->cidade}}</td>
              <td>{{ $registro->bairro}}</td>
              <td>{{ $registro->estado}}</td>
              <td>{{ $registro->cep}}</td>
              <td>{{ $registro->celular}}</td>


              <td>
                <a class="btn-small" href="{{ route('admin.eleitores.editar', $registro->id)}}">Editar</a>
                <a class="btn-small"  href="{{ route('admin.eleitores.consultar', $registro->id)}}">Ver</a>
                </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

    {{ $registros->links() }}

  </div>

</div>

@endsection
