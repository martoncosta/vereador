
<div class="login center">

  <h4 class="center"> Recuperar Senha</h4>

  <div class="row">

    <form method="POST" action="{{ url('/password/email') }}">

      {{ csrf_field() }}

          <div class="input-field col s4 offset-s4 card">
              <input id="email" type="email" name="email" value="{{ old('email') }}">
              <label><i class="material-icons left">email</i>E-mail</label>
          </div>

          @if ($errors->has('email'))

          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>

          @endif

      </div>

      <div class="row">
          <button type="submit" class="btn btn-login">
              <i class="material-icons left">email</i> Enviar Link de Recuperação de Senha
          </button>
      </div>

     </form>

  </div>
</div>
