@extends('layout.site')

@section('titulo','usuarios')

@section('conteudo')
  <div class="container">
    <h3 class="center">Editando usuarios</h3>
    <div class="row">

      <form class="" action="{{route('admin.usuarios.atualizar',$registro->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.usuarios._form_edita')

        <div class="row">
            <div class="col sm-2">

              <div class="col sm-2">
                <button type="submit" class="btn deep-blue">Salvar</button>
              </div>

              <div class="col sm-2">
                  <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
              </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.usuarios')}}">Cancelar</a>'
            </div>
        </div>

      </form>

    </div>
  </div>

@endsection
