
<div class="row">

  <div class="input-field col s4">
    <input type="text" name="name" value="{{isset($registro->name) ? $registro->name : ''}}">
    <label>Nome usuário</label>
  </div>

  <div class="input-field col s4">
    <input type="email" name="email" id="email" value="{{isset($registro->email) ? $registro->email : ''}}">
    <label>email</label>
  </div>

  <br /><br /><br /><br />
  <h5 class="center">Permissões do Usuário</h5>
  <br />


	<div class="checkbox">

      <div class="col s3">
        <input type="checkbox" name="cadastrar" id="cadastrar" value="{{ isset($registros->cadastrar) ? 1 : 2 }}" {{isset($registros->cadastrar) ? 'checked' : '' }}>
        <label for="cadastrar">Cadastrar</label>
      </div>

      <div class="col s3">
        <input type="checkbox" name="editar" id = "editar" value="{{ isset($registros->editar) ? 1 : 2 }}" {{isset($registros->editar) ? 'checked' : '' }}>
        <label for="editar">Editar</label>
      </div>

      <div class="col s3">
        <input type="checkbox" name="apagar" id = "apagar" value="{{ isset($registros->apagar) ? 1 : 2 }}" {{isset($registros->apagar) ? 'checked' : '' }} >
        <label for="apagar">apagar</label>
      </div>

      <div class="col s3">
        <input type="checkbox" name="config" id = "config" value="{{ isset($registros->config) ? 1 : 2 }}" {{isset($registros->config) ? 'checked' : '' }} >
        <label for="config">Config</label>
      </div>

      <br /><br /><br /><br />
  </div>
