@extends('layout.site')

@section('titulo','usuarios')

@section('conteudo')

<div class="container" align="center">

    <h5><i class="material-icons left">face</i>usuarios</h5>

    <div class="input-field col s12">
      <i class="material-icons prefix">search</i>
      <input id="txt_consulta" type="tel" class="validate">
      <label for="txt_consulta">Digite o texto para pesquisar</label>
    </div>

    <div class="row">
      <a class = "btn deep-blue" href="{{ route('admin.usuarios.adicionar')}}"><i class="material-icons left">add_circle</i>Adicionar</a>
    </div>

    <div class="row">
      <a> Total de usuarios : {{ $qtdregistros }}</a>
    </div>

  <div class="row">
      <table id="tabela" class="responsive-table striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>email</th>
            <th>A ç õ e s</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)
            <tr>
              <td>{{ $registro->id}}</td>
              <td>{{ $registro->name}}</td>
              <td>{{ $registro->email}}</td>

              <td>
                <a class="btn-small" href="{{ route('admin.usuarios.editar', $registro->id)}}">Editar  </a>
                <a class="btn-small" href="{{ route('admin.usuarios.deletar', $registro->id)}}">Excluir  </a>
              </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

    {{ $registros->links() }}

  </div>

</div>

@endsection
