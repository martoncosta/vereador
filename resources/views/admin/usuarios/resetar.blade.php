@extends('layout.site')

@section('titulo','usuarios')

@section('conteudo')

  <div class="container">

    <div class="row">

      <form class="class="login center"" action="{{''}}" method="post">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="put">

        @include('admin.usuarios._form_reset')

      </form>

    </div>
  </div>

@endsection
