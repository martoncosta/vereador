@extends('layout.site')

@section('titulo','usuarios')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar usuarios</h3>
    <div class="row">
      <form class="" action="{{route('admin.usuarios.salvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.usuarios._form')

        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.usuarios')}}">Cancelar</a>'
            </div>
        </div>
      </form>
    </div>
  </div>

@endsection
