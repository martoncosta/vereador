@extends('layout.site')

@section('titulo','Categoria cidadãos')

@section('conteudo')
  <div class="container">
    <h3 class="center">Editando Categoria cidadãos</h3>
    <div class="row">

      <form class="" action="{{route('admin.catpessoas.atualizar',$registro->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.catpessoas._form')

          <div class="row">
              <div class="col sm-2">
                  <button type="submit" class="btn deep-blue">Salvar</button>
              </div>

              <div class="col sm-2">
                  <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
              </div>

              <div class="col sm-2">
                  <a class="btn red"  href="{{route('admin.catpessoas')}}">Cancelar</a>'
              </div>
          </div>
         </form>

    </div>
  </div>

@endsection
