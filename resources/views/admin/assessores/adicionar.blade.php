@extends('layout.site')

@section('titulo','Assessores')

@section('conteudo')
  <div class="container">
    <h3 class="center">Adicionar Assessores</h3>
    <div class="row">
      <form class="" action="{{route('admin.assessores.salvar')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.assessores._form')
    
        <div class="row">
            <div class="col sm-2">
              <button type="submit" class="btn deep-blue">Salvar</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.assessores')}}">Cancelar</a>'
            </div>
        </div>
      </form>
    </div>
  </div>

@endsection
