@extends('layout.site')

@section('titulo','Assessores')

@section('conteudo')
  <div class="container">
    <h3 class="center">Editando Assessores</h3>
    <div class="row">

      <form class="" action="{{route('admin.assessores.atualizar',$registro->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.assessores._form')

        <div class="row">
            <div class="col sm-2">

              <div class="col sm-2">
                <button type="submit" class="btn deep-blue">Salvar</button>
              </div>

              <div class="col sm-2">
                  <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
              </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.assessores')}}">Cancelar</a>'
            </div>
        </div>

      </form>

    </div>
  </div>

@endsection
