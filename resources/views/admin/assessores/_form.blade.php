
<div class="row">

  <div class="input-field col s4">
    <input type="text" name="nomeassessor" value="{{isset($registro->nomeassessor) ? $registro->nomeassessor : ''}}">
    <label>Nome</label>
  </div>

  <div class="input-field col s4">
    <input type="text" name="endereco" id="endereco" value="{{isset($registro->endereco) ? $registro->endereco : ''}}">
    <label>Endereco</label>
  </div>

  <div class="col s3">

      <label for="catassessor"> Categoria assessor </label>

      <select name="catassessor" id="catassessor" class="browser-default" required placeholder="Selecione categoria">

        <option value="{{isset($registro->catassessor)}}" selected>{{isset($registro->catassessor) ? $regcatassessores[$registro->catassessor-1]->descatassessor: ''}}</option>

        @foreach( $regcatassessores as $catassessor )
          <option value="{{$catassessor->id}}">{{$catassessor->descatassessor}}</option>
        @endforeach

      </select>

</div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" name="bairro" id="bairro" value="{{isset($registro->bairro) ? $registro->bairro : ''}}">
      <label>Bairro</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="cidade" id="cidade" value="{{isset($registro->cidade) ? $registro->cidade : ''}}">
      <label>Cidade</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="estado" id="estado" value="{{isset($registro->estado) ? $registro->estado : ''}}">
      <label>UF</label>
    </div>

    <div class="input-field col s2">
        <input type="text" data-lenght="9" name="cep" id="cep" value="{{isset($registro->cep) ? $registro->cep : ''}}">
        <label>Cep</label>
      </div>

</div>

<div class="row">

    <div class="input-field col s3">
      <input type="text" name="telefone" id="telefone" value="{{isset($registro->telefone) ? $registro->telefone : ''}}">
      <label>Telefone</label>
    </div>

    <div class="input-field col s3">
      <input type="text" name="celular" id="celular" value="{{isset($registro->celular) ? $registro->celular : ''}}">
      <label>Celular</label>
    </div>

    <div class="input-field col s4">
      <input type="email" name="email" value="{{isset($registro->email) ? $registro->email : ''}}">
      <label>email</label>
    </div>

</div>

<div class="row">
    <div class="input-field col s11">
      <input type="text" name="observacoes" value="{{isset($registro->observacoes) ? $registro->observacoes : ''}}">
      <label>observacoes</label>
    </div>
</div>
