@extends('layout.site')

@section('titulo','Visitas')

@section('conteudo')

<div class="container" align="center">

  <h4><i class="material-icons left">business_center</i>Visitas</h4>

    <div class="input-field col s12">
      <i class="material-icons prefix">search</i>
      <input id="txt_consulta" type="tel" class="validate">
      <label for="txt_consulta">Digite o texto para pesquisar</label>
    </div>

    <div class="row">

      <a class = "btn deep-blue" href="{{ route('admin.visitas.adicionar')}}"><i class="material-icons left">add_circle</i>Adicionar</a>

    </div>
    <div class="row">
      <a> Total de visitas : {{ $qtdregistros }}</a>
    </div>

  <div class="row">
      <table id="tabela" class="responsive-table striped bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Descrição</th>
            <th>Local</th>
            <th>Data</th>
            <th>Horário</th>
            <th> A ç õ e s</th>
          </tr>
        </thead>

        <tbody>
          @foreach($registros as $registro)
            <tr>
              <td>{{ $registro->id}}</td>
              <td>{{ $registro->descricao}}</td>
              <td>{{ $registro->local}}</td>
              <td>{{ date( 'd/m/Y' , strtotime($registro->data))}} </td>
              <td>{{ $registro->horario}}</td>

              <td>
                <a class="btn-small" href="{{ route('admin.visitas.editar', $registro->id)}}">Editar / </a>
                <a class="btn-small" href="{{ route('admin.visitas.ata', $registro->id)}}">Ata</a>
              </td>

            </tr>
          @endforeach
        </tbody>
      </table>

  </div>

  <div class="row" align="center">

    {{ $registros->links() }}

  </div>

</div>

@endsection
