
<div class="row">

  <div class="input-field col s10">
    <input type="text" style="color: black" name="descricao" value="{{isset($registro->descricao) ? $registro->descricao : ''}}" disabled>
    <label>Descrição</label>
  </div>

</div>

<div class="row">

  <div class="input-field col s6">
    <input type="text" style="color: black" name="local" value="{{isset($registro->local) ? $registro->local : ''}}" disabled>
    <label>Local</label>
  </div>

</div>

<div class="row">

    <div class="col s4">
      <label for="date">Data</label>
      <input type="date" class="datepicker" id="datepickervisita" name="data" value="{{isset($registro->data) ? $registro->data : ''}}">
    </div>

</div>

<div class="row">

    <div class="input-field col s1">
        <input type="text" style="color: black" class="hora" id="hora" name="horario" value="{{isset($registro->horario) ? $registro->horario : ''}}" disabled>
        <label>Horário</label>

    </div>
</div>
