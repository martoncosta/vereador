
<div class="conteudo" id="printvisitas">

<div class="row">

  <div class="input-field col s10">
    <input type="text" name="descricao" value="{{isset($registro->descricao) ? $registro->descricao : ''}}">
    <label>Descrição</label>
  </div>

</div>

<div class="row">

  <div class="input-field col s6">
    <input type="text" name="local" value="{{isset($registro->local) ? $registro->local : ''}}">
    <label>Local</label>
  </div>

</div>

<div class="row">

    <div class="col s4">
      <label for="date">Data</label>
      <input type="date" class="datepicker" id="datepickervisita" name="data" value="{{isset($registro->data) ? $registro->data : ''}}">
    </div>

</div>

<div class="row">

    <div class="input-field col s4">
        <input type="text" class="hora" id="hora" name="horario" value="{{isset($registro->horario) ? $registro->horario : ''}}">
        <label>Horário</label>

    </div>
</div>

</div>
