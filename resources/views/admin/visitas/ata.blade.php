@extends('layout.site')

@section('titulo','Ata de visitas')

@section('conteudo')
  <div class="container">
    <h3 class="center">Ata da Visita</h3>
    <div class="row">

      <form class="" action="{{route('admin.visitas.imprimir',$registro->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @include('admin.visitas._form_ata')
        <div class="row">

            <div class="col sm-2">
              <button type="button" class="btn green" onclick="printBy();">Imprimir</button>
            </div>

            <div class="col sm-2">
              <a class="btn red"  href="{{route('admin.visitas')}}">Cancelar</a>'
            </div>
        </div>
      </form>

    </div>
  </div>

@endsection
