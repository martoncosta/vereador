<!--Import jQuery before materialize.js-->

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/pt-br.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<script type="text/javascript">

function printDiv()
 {
     var conteudo = document.getElementById('printvisitas').innerHTML;
     var win = window.open();
     win.document.write(conteudo);
     win.print();
     win.close();//Fecha após a impressão.
 }

</script>

<script type="text/javascript">

function printBy(selector){

    var $print = $(selector)
        .clone()
        .addClass('print')
        .prependTo('body');

    // Stop JS execution
    window.print();

    // Remove div once printed
    $print.remove();
}
</script>


<script type="text/javascript">

    var d = new Date(document.getElementById("datavisita" )),

        dia = '' + d.getDate(),
        ano = d.getFullYear();

    if (mes.length < 2) mes = '0' + mes;
    if (dia.length < 2) dia = '0' + dia;

    document.getElementById("datavisita").value = today;

</script>

<script type="text/javascript">
// RETORNA A DATA ATUAL para atualizar o cadastro da demanda na abertura e no fechamento//

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    var hor = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();

    if(dd<10){dd='0'+dd}

    if(mm<10){mm='0'+mm}

    today = yyyy+"-"+mm+"-"+dd+" "+hor+":"+min+":"+ss;

    document.getElementById("dtabertura").value = today;

</script>

<script type="text/javascript">
// RETORNA A DATA ATUAL para atualizar o cadastro da demanda na abertura e no fechamento//

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    var hor = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();

    if(dd<10){dd='0'+dd}

    if(mm<10){mm='0'+mm}

    today = yyyy+"-"+mm+"-"+dd+" "+hor+":"+min+":"+ss;

    document.getElementById("dtfechamento").value = today;

</script>

<script type="text/javascript">
  $(document).ready(function(){
    Materialize.updateTextFields();
    $(".button-collapse").sideNav();
  });
</script>

<script>

  $(document).ready(function(){

     $('select').material_select();

     $('.datepicker1').pickadate({
        selectMonths: true,
        selectYears: 130,
        // Título dos botões de navegação
        labelMonthNext: 'Próximo Mês',
        labelMonthPrev: 'Mês Anterior',
        // Título dos seletores de mês e ano
        labelMonthSelect: 'Selecione o Mês',
        labelYearSelect: 'Selecione o Ano',
        // Meses e dias da semana
        monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        // Letras da semana
        weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
        //Botões
        today: 'Hoje',
        clear: 'Limpar',
        close: 'Fechar',
        // Formato da data que aparece no input
        format: 'dd-mm-yyyy',
        onClose: function() {
      $(document.activeElement).blur()
      }
    });
  });

</script>

<script type="text/javascript">

    $('#categoria').on('change', function() {
        var id_categoria = $(this).val();

        // var path = '/acesso/public/';
        var path = "/code/Projeto%20vereador/vereador/public/";
        // alert(value);

        $.ajax({
            url: path + "retorna/subcategorias",
            type: 'post',
            data: {
                id_categoria:id_categoria
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',

            success: function (dados) {
                // console.log(dados.subcategorias[0].subcategoria);
                // document.getElementById('descricao').innerHTML=dados.subcategorias[key].id;

                $.each( dados.subcategorias, function( key ) {

                    // setup listener for custom event to re-initialize on change
                    $('.materialSelect').on('contentChanged', function() {
                        $(this).material_select();
                    });

                        var $newOpt = $("<option>").attr("value",dados.subcategorias[key].id).text(dados.subcategorias[key].subcategoria)
                        $("#myDropdown").append($newOpt);

                        // fire custom event anytime you've updated select
                        $("#myDropdown").trigger('contentChanged');

                });
            }
        });
    });


</script>


<!-- Mascaras para entrada de dados -->
<script type="text/javascript">

  $("#id_rua").select2();

  $("#hora").mask("00:00");
  $("#datepicker, #dtnascimento, #datapickervisita").mask("9999-99-99 99:99");

  $("#start1, #end1, #datepickerstart").mask("99-99-9999 99:99");

  $("#telefone").mask("(00) 0000-0000");
  $("#celular").mask("(00) 00000-0000");

  $("#telefoneelei").mask("(00) 0000-0000");
  $("#celularelei").mask("(00) 00000-0000");

  $("#cpf").mask("000.000.000-00");
  $("#identidade").mask("0000000000");

  $("#cep").mask("00000-000");

</script>
<!-- Timepicker -->
f<!-- Pesquisa em Tabelas com retorno de erro se nao achar-->
 <script type="text/javascript">

   $("input#txt_consulta").quicksearch("table#tabela tbody tr");

 </script>

<!-- Pesquisa de endereco por CEP -->
<!-- Adicionando Javascri  pt -->
<script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#endereco").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#estado").val("");
             }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#endereco").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#estado").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#endereco").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#estado").val(dados.uf);
                              } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

</script>

<script type="text/javascript">

  $(document).ready(function() {

    var evt = [];

    $.ajax({
      url: "compromissos/get",
      type: 'GET',
      dataType: 'JSON',
      async: false,

    }).done(function(r){

        evt = r;

    });

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listDay'
      },

      droppable: true,
      defaultDate: new Date(),
      editable: true,
      naveLinks: true,
      eventLimit: true,

      events: evt,

      dayClick: function(date, jsEvent, view, resourceObj) {

          alert('teste');

          $('#datainicial').text(date.format());
          $('#Evento').modal('show');
      },

      eventClick: function(calEvent, jsEvent, view) {

          console.log(calEvent);
          alert('Event: ' + calEvent.title);

          $(this).css('background', 'red');


      },

      editable: true,

      eventDrop: function(event, delta, revertFunc) {

          alert(event.title + "será alterado" + event.start.format());

          if (!confirm("Deseja trocar realmente?")) {

              revertFunc();

          }
      }


    });
  });

</script>

</body>
</html>
