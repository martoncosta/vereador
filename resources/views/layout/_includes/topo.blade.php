<!DOCTYPE html>
<html>
<head>
  <title>@yield('titulo')</title>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->

  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <!-- Style for this kind of example-->
  <style>

  @media print {
      html, body {
          margin: 0;
          padding: 0;
          border: 0;
      }
      #printable {
          margin: 0;
          padding: 0;
          border: 0;
          font-size: 14px;
      }
      #printable ~ * {
          display: none;
      }
  }

  textarea {
    width: 1000px;
    height: 105px;
  }

  nav {
        width: 1050px;
        height: 50px;
        line-height: 50px;
    }

  .nav-wrapper  {
      font-size: 15px;
    }

  .dropdown-button  {
      font-size: 15px;

    }
  </style>

</head>
<body>
<header>

<div class="container">

  <nav>
   <div class="nav-wrapper green">

       <a href="#!" class="brand-logo"></a>
       <a href="#" data-activates="mobile" class="button-collapse"><i class="material-icons">menu</i></a>
       <ul class="right hide-on-med-and-down">

         <li><a href="{{ route('site.home')}}"><i class="material-icons left">home</i>Home</a></li>

         @if(Auth::guest())
            <li><a href="{{ route('site.login')}}"><i class="material-icons left">account_box</i>Login</a></li>
         @else

           <li><a class="dropdown-button" href="{{ route('admin.dashboard')}}"><i class="material-icons left">dashboard</i>Resumo</a></li>

           <li><a class="dropdown-button" href="#!" data-activates="dropdown3"><i class="material-icons left">face</i>Cadastros</a></li>
           <li><a class="dropdown-button" href="#!" data-activates="dropdown1"><i class="material-icons left">record_voice_over</i>Demandas</a></li>
           <li><a class="dropdown-button" href="#!" data-activates="dropdown2"><i class="material-icons left">local_library</i>Agenda</a></li>
           <li><a class="dropdown-button" href="#!" data-activates="dropdown4"><i class="material-icons left">account_circle</i>Config</a></li>
           <li><a href=" {{ route('site.login.sair')}}"><i class="material-icons left">exit_to_app</i>Sair</a></li>

           <li><a href="#"><i class="material-icons left">person</i>{{ Auth::user()->name}}</a></li>

           <!-- Dropdown Structures-->
           <!-- Dropdown Cadastros-->
           <ul id="dropdown3" class="dropdown-content">
             <li><a style="color: black;" href="{{ route('admin.assessores')}}">Assessor</a></li>
             <li><a style="color: black;" href="{{ route('admin.eleitores')}}">Cidadãos</a></li>
             <li class="divider"></li>
             <li><a style="color: black;" href="{{ route('admin.catpessoas')}}">Tipo cidadão</a></li>
             <li><a style="color: black;" href="{{ route('admin.catassessores')}}">Tipo Assessor</a></li>
             <li><a style="color: black;" href="#">Aniversarios</a></li>
           </ul>

           <!-- Demandas-->
          <ul id="dropdown1" class="dropdown-content">
            <li><a style="color: black;" href="{{ route('admin.visitas')}}">Visitas</a></li>

            <li class="divider"></li>
            <li><a style="color: orange;" href="#">D E M A N D A S</a></li>
            <li><a style="color: black;" href="{{ route('admin.demandas')}}">Abertas</a></li>
            <li><a style="color: black;" href="{{ route('admin.demandas.fechadas')}}">Fechadas</a></li>

            <li class="divider"></li>
            <li><a style="color: orange;" href="#">T I P O S</a></li>
            <li><a style="color: black;" href="{{ route('admin.categorias')}}">Categoria</a></li>
            <li><a style="color: black;" href="{{ route('admin.subcategorias')}}">SubCategoria</a></li>
          </ul>

          <!-- Dropdown agenda-->
          <ul id="dropdown2" class="dropdown-content">
            <li><a style="color: black;" href="{{ route('admin.compromissos')}}">Compromissos</a></li>
            <li class="divider"></li>
            <li><a style="color: black;" href="{{ route('site.home')}}">Calendário</a></li>
            <li class="divider"></li>
            <li><a style="color: black;" href="#">Definir</a></li>
          </ul>

          <!-- Dropdown Config-->
          <ul id="dropdown4" class="dropdown-content">
            <li><a style="color: black;" href="{{ route('admin.usuarios')}}">Usuários</a></li>
            <li class="divider"></li>
            <li><a style="color: black;" href="{{ route('site.home')}}">Email</a></li>
            <li class="divider"></li>
            <li><a style="color: black;" href="#">Etiquetas</a></li>
          </ul>


         @endif

       </ul>

       <ul class="side-nav" id="mobile">
        <li><a href="/">Home</a></li>

        @if(Auth::guest())
              <li><a href=" {{ route('site.login')}}">Login</a></li>
        @else
              <li><a href="{{ route('admin.eleitores')}}">Eleitor</a></li>
              <li><a href="{{ route('admin.visitas')}}">Visitas</a></li>
              <li><a href="{{ route('admin.demandas')}}">Demandas</a></li>
              <li><a href="{{ route('admin.compromissos')}}">Compromissos</a></li>
              <li><a href=" {{ route('site.login.sair')}}"><i class="material-icons left">exit_to_app</i>Sair</a></li>
              <li><a href="#"><i class="material-icons left">person</i>{{ Auth::user()->name}}</a></li>

          @endif
       </ul>

     </div>
   </div>

 </nav>
</header>
