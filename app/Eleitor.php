<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eleitor extends Model
{
  protected $table = 'eleitors';

  protected $fillable = [
      'tipopessoa','nome', 'endereco', 'bairro','cidade','estado','cep','dtnascimento','identidade','cpf','telefone', 'celular','email',  'assessor', 'observacoes',
];

    public function assessores(){
        return $this->hasMany(Assessor::class);
    }

}
