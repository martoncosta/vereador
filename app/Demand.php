<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
  protected $fillable = [
      'databertura', 'datafechamento', 'status','categoria','subcategoria','endereco','bairro','descricao','solucao','prazo','eleitor', 'assessor',
  ];


  public function categoria(){
      return $this->hasMany('app\models\Categor', 'descricao','id');
  }

  public function subcategoria(){
      return $this->hasMany('app\models\Subcategor', 'subcategoria','id');
  }

  public function assessor(){
      return $this->hasMany('app\models\Assessor', 'assessor','id');
  }

  public function eleitor(){
      return $this->hasMany('app\models\Eleitor', 'eleitor','id');
  }

  public function logdemand(){
      return $this->hasMany('app\models\Logdemand', 'demandaid','id');
  }


}
