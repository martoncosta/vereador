<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compromissos extends Model
{
  protected $fillable = [
      'id', 'title','descricao','color', 'start','end',
  ];
}
