<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categor extends Model
{

  protected $table = 'categors';

  protected $fillable = [
      'descricaocategoria'
  ];

}
