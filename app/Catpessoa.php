<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catpessoa extends Model
{

  protected $table = 'catpessoas';

  protected $fillable = [
      'descatpessoa'
  ];

}
