<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessor extends Model
{

  protected $table = 'assessors';

  protected $fillable = [
      'nomeassessor', 'catassessor','endereco', 'bairro','cidade','estado','cep','telefone','celular','email','observacoes',
  ];

  public function eleitores(){
        return $this->belongsToMany(Eleitor::class);
    }
}
