<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategor extends Model
{

  protected $table = 'subcategors';

  protected $fillable = [
      'categoria_id','subcategoria'
  ];

  public function categorias(){
        return $this->belongsToMany(Categor::class);
    }
}
