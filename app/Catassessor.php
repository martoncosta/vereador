<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catassessor extends Model
{

  protected $table = 'catassessors';

  protected $fillable = [
      'descatassessor'
  ];

}
