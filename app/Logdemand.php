<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logdemand extends Model
{

  protected $table = 'logdemands';

  protected $fillable = [
      'demandasid', 'logdescricao',
  ];

  
}
