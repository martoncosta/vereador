<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Compromissos;
use App\Demand;
use App\Subcategor;
use App\Categor;
use App\Assessor;
use App\Eleitor;
use App\Visit;

class DashboardController extends Controller
{
    public function index()
    {

        /* Historico total */
        $regassessores = Assessor::count();
        $regeleitores = Eleitor::count();
        $regdemandasabertas = Demand::where('demands.status', '=', 'aberto')->count();
        $regdemandasfechadas = Demand::where('demands.status', '=' ,'fechado')->count();
        $regcompromissos = Compromissos::count();
        $regvisitas = Visit::count();
        $regcompromissos = Compromissos::count();

        /* Historico de Demandas */
        $regdemandasjan = Demand::whereMonth('demands.created_at', '=', '01')->count();
        $regdemandasfev = Demand::whereMonth('demands.created_at', '=', '02')->count();
        $regdemandasmar = Demand::whereMonth('demands.created_at', '=', '03')->count();
        $regdemandasabr = Demand::whereMonth('demands.created_at', '=', '04')->count();
        $regdemandasmai = Demand::whereMonth('demands.created_at', '=', '05')->count();
        $regdemandasjun = Demand::whereMonth('demands.created_at', '=', '06')->count();
        $regdemandasjul = Demand::whereMonth('demands.created_at', '=', '07')->count();
        $regdemandasago = Demand::whereMonth('demands.created_at', '=', '08')->count();
        $regdemandasset = Demand::whereMonth('demands.created_at', '=', '09')->count();
        $regdemandasout = Demand::whereMonth('demands.created_at', '=', '10')->count();
        $regdemandasnov = Demand::whereMonth('demands.created_at', '=', '11')->count();
        $regdemandasdez = Demand::whereMonth('demands.created_at', '=', '12')->count();

        /* Historico de visitas */
        $regvisitasjan = Visit::whereMonth('visits.data', '=', '01')->count();
        $regvisitasfev = Visit::whereMonth('visits.data', '=', '02')->count();
        $regvisitasmar = Visit::whereMonth('visits.data', '=', '03')->count();
        $regvisitasabr = Visit::whereMonth('visits.data', '=', '04')->count();
        $regvisitasmai = Visit::whereMonth('visits.data', '=', '05')->count();
        $regvisitasjun = Visit::whereMonth('visits.data', '=', '06')->count();
        $regvisitasjul = Visit::whereMonth('visits.data', '=', '07')->count();
        $regvisitasago = Visit::whereMonth('visits.data', '=', '08')->count();
        $regvisitasset = Visit::whereMonth('visits.data', '=', '09')->count();
        $regvisitasout = Visit::whereMonth('visits.data', '=', '10')->count();
        $regvisitasnov = Visit::whereMonth('visits.data', '=', '11')->count();
        $regvisitasdez = Visit::whereMonth('visits.data', '=', '12')->count();

        /* Historico de visitas */
        $regcomprojan = Compromissos::whereMonth('compromissos.start', '=', '01')->count();
        $regcomprofev = Compromissos::whereMonth('compromissos.start', '=', '02')->count();
        $regcompromar = Compromissos::whereMonth('compromissos.start', '=', '03')->count();
        $regcomproabr = Compromissos::whereMonth('compromissos.start', '=', '04')->count();
        $regcompromai = Compromissos::whereMonth('compromissos.start', '=', '05')->count();
        $regcomprojun = Compromissos::whereMonth('compromissos.start', '=', '06')->count();
        $regcomprojul = Compromissos::whereMonth('compromissos.start', '=', '07')->count();
        $regcomproago = Compromissos::whereMonth('compromissos.start', '=', '08')->count();
        $regcomproset = Compromissos::whereMonth('compromissos.start', '=', '09')->count();
        $regcomproout = Compromissos::whereMonth('compromissos.start', '=', '10')->count();
        $regcompronov = Compromissos::whereMonth('compromissos.start', '=', '11')->count();
        $regcomprodez = Compromissos::whereMonth('compromissos.start', '=', '12')->count();

        /* Historico de Eleitores */
        $regeleitorjan = Eleitor::whereMonth('eleitors.created_at', '=', '01')->count();
        $regeleitorfev = Eleitor::whereMonth('eleitors.created_at', '=', '02')->count();
        $regeleitormar = Eleitor::whereMonth('eleitors.created_at', '=', '03')->count();
        $regeleitorabr = Eleitor::whereMonth('eleitors.created_at', '=', '04')->count();
        $regeleitormai = Eleitor::whereMonth('eleitors.created_at', '=', '05')->count();
        $regeleitorjun = Eleitor::whereMonth('eleitors.created_at', '=', '06')->count();
        $regeleitorjul = Eleitor::whereMonth('eleitors.created_at', '=', '07')->count();
        $regeleitorago = Eleitor::whereMonth('eleitors.created_at', '=', '08')->count();
        $regeleitorset = Eleitor::whereMonth('eleitors.created_at', '=', '09')->count();
        $regeleitorout = Eleitor::whereMonth('eleitors.created_at', '=', '10')->count();
        $regeleitornov = Eleitor::whereMonth('eleitors.created_at', '=', '11')->count();
        $regeleitordez = Eleitor::whereMonth('eleitors.created_at', '=', '12')->count();

        /* Historico de Assessores */
        $regassessorjan = Assessor::whereMonth('assessors.created_at', '=', '01')->count();
        $regassessorfev = Assessor::whereMonth('assessors.created_at', '=', '02')->count();
        $regassessormar = Assessor::whereMonth('assessors.created_at', '=', '03')->count();
        $regassessorabr = Assessor::whereMonth('assessors.created_at', '=', '04')->count();
        $regassessormai = Assessor::whereMonth('assessors.created_at', '=', '05')->count();
        $regassessorjun = Assessor::whereMonth('assessors.created_at', '=', '06')->count();
        $regassessorjul = Assessor::whereMonth('assessors.created_at', '=', '07')->count();
        $regassessorago = Assessor::whereMonth('assessors.created_at', '=', '08')->count();
        $regassessorset = Assessor::whereMonth('assessors.created_at', '=', '09')->count();
        $regassessorout = Assessor::whereMonth('assessors.created_at', '=', '10')->count();
        $regassessornov = Assessor::whereMonth('assessors.created_at', '=', '11')->count();
        $regassessordez = Assessor::whereMonth('assessors.created_at', '=', '12')->count();

        return View('admin.dashboard.visualiza', compact('regassessores','regeleitores','regdemandasfechadas','regdemandasabertas','regcompromissos','regcompromissos','regvisitas',
        'regdemandasjan','regdemandasfev','regdemandasmar','regdemandasabr','regdemandasmai','regdemandasjun','regdemandasjul','regdemandasago','regdemandasset','regdemandasout','regdemandasnov','regdemandasdez',
        'regvisitasjan','regvisitasfev','regvisitasmar','regvisitasabr','regvisitasmai','regvisitasjun','regvisitasjul','regvisitasago','regvisitasset','regvisitasout','regvisitasnov','regvisitasdez',
        'regcomprojan','regcomprofev','regcompromar','regcomproabr','regcompromai','regcomprojun','regcomprojul','regcomproago','regcomproset','regcomproout','regcompronov','regcomprodez',
        'regeleitorjan','regeleitorfev','regeleitormar','regeleitorabr','regeleitormai','regeleitorjun','regeleitorjul','regeleitorago','regeleitorset','regeleitorout','regeleitornov','regeleitordez',
        'regassessorjan','regassessorfev','regassessormar','regassessorabr','regassessormai','regassessorjun','regassessorjul','regassessorago','regassessorset','regassessorout','regassessornov','regassessordez'

      ));

    }

}
