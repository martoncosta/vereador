<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Visit;

class VisitasController extends Controller
{
    public function index(){

        $registros = Visit::orderBy('data', 'desc')->paginate(100);
        $qtdregistros = Visit::count();

        return view('admin.visitas.index', compact('registros', 'qtdregistros'));
    }

    public function adicionar(){

      return view('admin.visitas.adicionar', compact('regassessores'));

    }

    public function salvar(Request $req){

        $dados = $req->all();

        Visit::create($dados);
        return redirect()->route('admin.visitas');
    }

    public function editar($id){

      $registro = Visit::find($id);

      return view('admin.visitas.editar',compact('registro'));

    }


    public function ata($id){

      $registro = Visit::find($id);

      return view('admin.visitas.ata',compact('registro'));

    }


    public function atualizar(Request $req, $id){

      $dados = $req->all();
      Visit::find($id)->update($dados);
      return redirect()->route('admin.visitas');

    }

    public function deletar($id){

    Visit::find($id)->delete();
    return redirect()->route('admin.visitas');

    }

    public function consultar($id){

    $registro = Visit::find($id);

    return view('admin.visitas.consultar',compact('registro'));

    }

    public function dataVisita($datavisita)
       {
           $value = $datavisita->data;
           if ($value)
           {
               return $value->format('d/m/Y');
           }
           return null;
       }


}
