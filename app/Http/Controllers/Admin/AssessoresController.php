<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Assessor;
use App\Catassessor;

class AssessoresController extends Controller
{
    public function index(){

        $registros = Assessor::OrderBy('nomeassessor')->paginate(20);
        $regcatassessores = catassessor::all();
        $qtdregistros = Assessor::count();

        return view('admin.assessores.index', compact('registros','qtdregistros','regcatassessores'));
    }

    public function adicionar(){

      $regcatassessores = catassessor::all();
      return view('admin.assessores.adicionar', compact('regcatassessores'));

    }

    public function salvar(Request $req){
        $dados = $req->all();
        Assessor::create($dados);
        return redirect()->route('admin.assessores');
    }

    public function editar($id){
      $registro = Assessor::find($id);
      $regcatassessores = catassessor::all();
      return view('admin.assessores.editar',compact('registro','regcatassessores'));
    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();

      Assessor::find($id)->update($dados);
      return redirect()->route('admin.assessores');

    }

    public function deletar($id){

    Assessor::find($id)->delete();
    return redirect()->route('admin.assessores');

    }


}
