<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Catpessoa;

class CatpessoasController extends Controller
{
    public function index(){

        $registros = Catpessoa::OrderBy('descatpessoa')->paginate(100);
        return view('admin.catpessoas.index', compact('registros'));
    }

    public function adicionar(){

      return view('admin.catpessoas.adicionar');

    }

    public function salvar(Request $req){
        $dados = $req->all();
        Catpessoa::create($dados);
        return redirect()->route('admin.catpessoas');
    }

    public function editar($id){

      $registro = Catpessoa::find($id);

      return view('admin.catpessoas.editar',compact('registro'));

    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();
      Catpessoa::find($id)->update($dados);
      return redirect()->route('admin.catpessoas');

    }

    public function deletar($id){

    Catpessoa::find($id)->delete();
    return redirect()->route('admin.catpessoas');

    }

    public function consultar($id){

    $registro = Catpessoa::find($id);

    return view('admin.catpessoas.consultar',compact('registro'));

    }

}
