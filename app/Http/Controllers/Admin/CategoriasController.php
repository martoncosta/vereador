<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categor;

class categoriasController extends Controller
{
    public function index(){

        $registros = Categor::OrderBy('descricaocategoria')->paginate(100);
        return view('admin.categorias.index', compact('registros'));
    }

    public function adicionar(){

      return view('admin.categorias.adicionar');

    }

    public function salvar(Request $req){
        $dados = $req->all();
        Categor::create($dados);
        return redirect()->route('admin.categorias');
    }

    public function editar($id){

      $registro = Categor::find($id);

      return view('admin.categorias.editar',compact('registro'));

    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();
      Categor::find($id)->update($dados);
      return redirect()->route('admin.categorias');

    }

    public function deletar($id){

    Categor::find($id)->delete();
    return redirect()->route('admin.categorias');

    }

    public function consultar($id){

    $registro = Categor::find($id);

    return view('admin.categorias.consultar',compact('registro'));

    }

}
