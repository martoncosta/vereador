<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Eleitor;
use App\Assessor;
use App\Catpessoa;

class EleitoresController extends Controller
{
    public function index(){

        $registros = Eleitor::paginate(6000);
        $regassessores = Assessor::all();
        $regtipopessoa = Catpessoa::all();

        $qtdregistros = Eleitor::count();


        return view('admin.eleitores.index', compact('registros','regassessores','qtdregistros','regtipopessoa'));
    }

    public function adicionar(){

      $regassessores = Assessor::all();
      $regtipopessoa = Catpessoa::all();

      return view('admin.eleitores.adicionar', compact('regassessores','regtipopessoa'));

    }

    public function salvar(Request $req){
        $dados = $req->all();
        Eleitor::create($dados);
        return redirect()->route('admin.eleitores');
    }

    public function editar($id){
      $registro = Eleitor::find($id);
      $regassessores = Assessor::all();
      $regtipopessoa = Catpessoa::all();

      return view('admin.eleitores.editar',compact('registro','regassessores','regtipopessoa'));

    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();
      Eleitor::find($id)->update($dados);
      return redirect()->route('admin.eleitores');

    }

    public function deletar($id){

    Eleitor::find($id)->delete();
    return redirect()->route('admin.eleitores');

    }

    public function consultar($id){

    $registro = Eleitor::find($id);
    $regassessores = Assessor::all();
    $regtipopessoa = Catpessoa::all();

    return view('admin.eleitores.consultar',compact('registro','regassessores','regtipopessoa'));

    }

}
