<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Catassessor;

class CatassessoresController extends Controller
{
    public function index(){

        $registros = Catassessor::OrderBy('descatassessor')->paginate(100);
        return view('admin.catassessores.index', compact('registros'));
    }

    public function adicionar(){

      return view('admin.catassessores.adicionar');

    }

    public function salvar(Request $req){
        $dados = $req->all();
        Catassessor::create($dados);
        return redirect()->route('admin.catassessores');
    }

    public function editar($id){

      $registro = Catassessor::find($id);

      return view('admin.catassessores.editar',compact('registro'));

    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();
      Catassessor::find($id)->update($dados);
      return redirect()->route('admin.catassessores');

    }

    public function deletar($id){

    Catassessor::find($id)->delete();
    return redirect()->route('admin.catassessores');

    }

    public function consultar($id){

    $registro = catassessor::find($id);

    return view('admin.catassessores.consultar',compact('registro'));

    }

}
