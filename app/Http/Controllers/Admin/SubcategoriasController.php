<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subcategor;
use App\Categor;

class SubcategoriasController extends Controller
{
    public function index(){

        $registros = Subcategor::OrderBy('subcategoria')->paginate(100);
        $regcategorias = Categor::all();
        
        $qtdregistros = SubCategor::count();

        return view('admin.subcategorias.index', compact('registros','regcategorias','qtdregistros'));
    }

    public function adicionar(){

      $regcategorias = Categor::all();

      return view('admin.subcategorias.adicionar', compact('regcategorias'));

    }

    public function salvar(Request $req){
        $dados = $req->all();
        Subcategor::create($dados);
        return redirect()->route('admin.subcategorias');
    }

    public function editar($id){
      $registros = Subcategor::find($id);
      $regcategorias = Categor::all();

      return view('admin.subcategorias.editar',compact('registros'),compact('regcategorias'));
    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();

      Subcategor::find($id)->update($dados);

      return redirect()->route('admin.subcategorias');

    }

    public function deletar($id){

    Subcategor::find($id)->delete();
    return redirect()->route('admin.subcategorias');

    }

    public function consultar($id){

    $registro = Subcategor::find($id);
    $regcategorias = Categor::all();

    return view('admin.subcategorias.consultar',compact('registro'), compact('regcategorias'));

    }

}
