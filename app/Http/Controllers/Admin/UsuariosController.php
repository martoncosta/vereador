<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsuariosController extends Controller
{
    public function index(){

        $registros = User::OrderBy('name')->paginate(20);
        $qtdregistros = User::count();

        return view('admin.usuarios.index', compact('registros','qtdregistros'));
    }

    public function adicionar(){

      $registros = User::all();

      return view('admin.usuarios.adicionar', compact('registros'));

    }

    public function salvar(Request $req){

        $dados = $req->all();

        $dados['password'] = bcrypt($dados['password']);

        User::create($dados);

        return redirect()->route('admin.usuarios');
    }

    public function editar($id){

      $registro = User::find($id);

      return view('admin.usuarios.editar',compact('registro'));
    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();

      User::find($id)->update($dados);
      return redirect()->route('admin.usuarios');

    }

    public function deletar($id){

    User::find($id)->delete();
    return redirect()->route('admin.usuarios');

    }


}
