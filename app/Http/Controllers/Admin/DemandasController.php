<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Demand;
use App\Subcategor;
use App\Categor;
use App\Assessor;
use App\Eleitor;
use App\Logdemand;

use Illuminate\Support\Facades\DB;

class DemandasController extends Controller
{
    public function index_abertas(){

        //Monta uma query de pesquisas integrando tabelas demandas, categorias, subcategorias, eleitores, regassessores

        $registros = Demand::join('categors','demands.categoria', '=', 'categors.id')
        ->join('subcategors','demands.subcategoria', '=', 'subcategors.id')
        ->join('eleitors','demands.eleitor', '=', 'eleitors.id')
        ->join('assessors','demands.assessor', '=', 'assessors.id')
        ->select('demands.prazo','demands.status','demands.databertura','demands.id','categors.descricaocategoria','subcategors.subcategoria','demands.endereco','demands.bairro','demands.descricao','eleitors.nome','assessors.nomeassessor','eleitors.id as eleitors_id','assessors.id as assessors_id','categors.id as categors_id','subcategors.id as subcategors_id')
        ->where('demands.status','=','aberto')
        ->orderby('demands.databertura','desc')
        ->get();

        $registros1 = Demand::orderBy('databertura', 'desc')->paginate(500);

        $regcategorias = Categor::all();
        $regsubcategorias = Subcategor::all();

        $regassessores = Assessor::all();
        $regeleitores = Eleitor::all();

        $qtdregistros = Demand::where('status','=','aberto')->count();

        return View('admin.demandas.index_abertas', compact('registros','qtdregistros','regcategorias','regsubcategorias','regassessores', 'regeleitores'));

    }

    public function index_fechadas(){

        //Monta uma query de pesquisas integrando tabelas demandas, categorias, subcategorias, eleitores, regassessores

        $registros = Demand::join('categors','demands.categoria', '=', 'categors.id')
        ->join('subcategors','demands.subcategoria', '=', 'subcategors.id')
        ->join('eleitors','demands.eleitor', '=', 'eleitors.id')
        ->join('assessors','demands.assessor', '=', 'assessors.id')
        ->select('demands.prazo','demands.status','demands.databertura','demands.id','categors.descricaocategoria','subcategors.subcategoria','demands.datafechamento','demands.endereco','demands.bairro','demands.descricao','demands.solucao','eleitors.nome','assessors.nomeassessor','eleitors.id as eleitors_id','assessors.id as assessors_id','categors.id as categors_id','subcategors.id as subcategors_id')
        ->where('demands.status','=','fechado')
        ->orderby('demands.databertura','desc')
        ->get();

        $regcategorias = Categor::all();
        $regsubcategorias = Subcategor::all();

        $regassessores = Assessor::all();
        $regeleitores = Eleitor::all();

        $qtdregistros = Demand::where('status','=','fechado')->count();

        return View('admin.demandas.index_fechadas', compact('registros','qtdregistros','regcategorias','regsubcategorias','regassessores', 'regeleitores'));

    }

    public function index_logdemandas($id){

        //Monta uma query de pesquisas integrando tabelas demandas, categorias, subcategorias, eleitores, regassessores
        $iddemand = $id;

        $registros = Logdemand::select('logdemands.id','logdemands.created_at','logdemands.logdescricao')
        ->where('logdemands.demandasid','=', $iddemand)
        ->orderby('logdemands.created_at','desc')
        ->get();

        $qtdregistros = Logdemand::where('logdemands.demandasid','=',$iddemand)->count();

        return View('admin.demandas.index_logdemandas', compact('registros','qtdregistros','iddemand'));

    }

    public function retorna_subcategorias(Request $req){
    //Rotina criada pelo Northon para pegar subcategorias e retornar Json recuperado na view com Ajax

        $subcategorias = DB::table('subcategors')
            ->select('*')
            ->where('categoria_id', '=', $req->id_categoria)
            ->get();

        return response()->json([
            'subcategorias' => $subcategorias
        ]);

    }

    public function adicionar(){

      $regcategorias = Categor::all();
      $regsubcategorias = Subcategor::all();
      $regassessores = Assessor::all();
      $regeleitores = Eleitor::all();

      return View('admin.demandas.adicionar', compact('regcategorias','regsubcategorias','regassessores','regeleitores'));

    }

    public function salvar(Request $req){

        $dados = $req->all();

        Demand::create($dados);

        return redirect()->route('admin.demandas');
    }

    public function logsalvar(Request $req){

        $dados = $req->all();
        $iddemand = $req->demandasid;

        Logdemand::create($dados);

        //Redireciona para o index Log Demandas //
        $registros = Logdemand::select('logdemands.id','logdemands.created_at','logdemands.logdescricao')
        ->where('logdemands.demandasid','=', $iddemand)
        ->orderby('logdemands.created_at','desc')
        ->get();

        $qtdregistros = Logdemand::where('logdemands.demandasid','=', $iddemand)->count();

        return View('admin.demandas.index_logdemandas', compact('registros','qtdregistros','iddemand'));

        //return redirect()->route('admin.logdemandas','iddemands');//
    }

    public function editar($id){

      $registros = Demand::find($id);
      $regcategorias = Categor::all();
      $regsubcategorias = Subcategor::all();
      $regassessores = Assessor::all();
      $regeleitores = Eleitor::all();

      return View('admin.demandas.editar', compact('registros','regcategorias','regsubcategorias','regassessores','regeleitores'));

    }

    public function fechar($id){
    //Fechar a demanda passando o Status para fechado e gravando a data do fechamento //

      $registros = Demand::find($id);
      $regcategorias = Categor::all();
      $regsubcategorias = Subcategor::all();
      $regassessores = Assessor::all();
      $regeleitores = Eleitor::all();

      return View('admin.demandas.fechar', compact('registros','regcategorias','regsubcategorias','regassessores','regeleitores'));

    }

    public function reabre($id){
    //Fechar a demanda passando o Status para fechado e gravando a data do fechamento //

      $registros = Demand::find($id);
      $regcategorias = Categor::all();
      $regsubcategorias = Subcategor::all();
      $regassessores = Assessor::all();
      $regeleitores = Eleitor::all();

      return View('admin.demandas.reabre', compact('registros','regcategorias','regsubcategorias','regassessores','regeleitores'));

    }

    public function logadicionar($id){

          $iddemands = $id;

          return View('admin.demandas.logadicionar', compact('iddemands'));

    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();

      Demand::find($id)->update($dados);
      return redirect()->route('admin.demandas');

    }

    public function logdeletar($id, $id1){

    Logdemand::find($id)->delete();

    $iddemand = $id1;

    //Redireciona para o index Log Demandas //
    $registros = Logdemand::select('logdemands.id','logdemands.created_at','logdemands.logdescricao')
    ->where('logdemands.demandasid','=', $iddemand)
    ->orderby('logdemands.created_at','desc')
    ->get();

    $qtdregistros = Logdemand::where('logdemands.demandasid','=',$id1)->count();

    return View('admin.demandas.index_logdemandas', compact('registros','qtdregistros','iddemand'));

    //return redirect()->route('admin.logdemandas','iddemands');//

    }

    public function consultar($id){

      $registros = Demand::find($id);
      $regcategorias = Categor::all();
      $regsubcategorias = Subcategor::all();
      $regassessores = Assessor::all();
      $regeleitores = Eleitor::all();

      return View('admin.demandas.consultar', compact('registros','regcategorias','regsubcategorias','regassessores','regeleitores'));

    }

}
