<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Compromissos;

class CompromissosController extends Controller
{
    public function index(){

        $registros = Compromissos::orderBy('start', 'desc')->paginate(100);
        $qtdregistros = Compromissos::count();

        return view('admin.compromissos.index', compact('registros', 'qtdregistros'));
    }

    public function adicionar(){

      return view('admin.compromissos.adicionar');

    }

    public function salvar(Request $req){

        $dados = $req->all();

        Compromissos::create($dados);

        return redirect()->route('admin.compromissos');

    }

    public function editar($id){

      $registro = Compromissos::find($id);

      return view('admin.compromissos.editar',compact('registro'));

    }

    public function atualizar(Request $req, $id){

      $dados = $req->all();

      /* Formatar data para gravação */
      $dados['start'] = date('Y-m-d H:i', strtotime($dados['start']));
      $dados['end'] = date('Y-m-d H:i', strtotime($dados['end']));

      Compromissos::find($id)->update($dados);

      return redirect()->route('admin.compromissos');

    }

    public function deletar($id){

    Compromissos::find($id)->delete();
    return redirect()->route('admin.compromissos');

    }

    public function consultar($id){


    $registro = Compromissos::find($id);

    return view('admin.compromissos.consultar',compact('registro'));

    }

    function formatarData($data) {

            $d = new Date($data);
            $dia = '' + $d.getDate();
            $ano = $d.getFullYear();

        if ($mes.length < 2) $mes = '0' + $mes;
        if ($dia.length < 2) $dia = '0' + $dia;

        return [$ano, $mes, $dia].join('-'); // "join" é o caracter para separar a formatação da data, neste caso, a barra (/)
    }

}
