<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{

    public function index()
    {
      return view('login.index');
    }

    public function entrar(Request $req)
    {

      $dados = $req->all();

      if(Auth::attempt(['email'=>$dados['email'],'password'=>$dados['senha']])){
        return redirect()->route('site.home');
      }else
      {
       return redirect()->route('site.login');
      /*return redirect()->route('site.login');*/
      }
    }

    public function resetar()
    {

        return view('admin.usuarios.resetar');

    }

    public function sair()
    {
      Auth::logout();
      return redirect()->route('site.home');
    }

    /* Mostra a troca de senhas */
    public function emailPassword(){
            return view('auth.passwords.email');
    }

    /* Mostra a troca de senhas */
    public function showChangePasswordForm(){
            return view('auth.changepassword');
        }

        /* Mostra a troca de senhas */
    public function sendPassword(){
                return view('auth.changepassword');
        }


    /* Troca a senha redirecioada do email */
    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

    }

}
