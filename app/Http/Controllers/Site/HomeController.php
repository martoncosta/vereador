<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Assessor;
use App\Compromissos;

class HomeController extends Controller
{
   /*
    * Create a new controller instance.
    *
    * @return void
    */

    public function __construct()
    {
          $this->middleware('auth');
    }


    public function index()
    {
      /* $cursos = Curso::all() --> Pega todos os registros do banco sem paginação */

      /* Paginacao com comando paginate 2 registros por pagina
      $cursos = Curso::paginate(2);
      return view('home',compact('cursos'));*/
      return view('home');
    }

    /* Trocar senha */

    /* Retorna o registro de compromissos do FullCalendar*/
    public function get_events(){

      $events = Compromissos::select("id", "title", "color", "start", "end")->get()->toArray();
      return response()->json($events);

    }

    /* Criacao de eventos do FullCalendar */
    public function create_events(Request $request) {

        $input = $request->all();

        $input["start"] = $input["start"]." ".date("H:m:s", strtotime($input["start"]));
        $input["end"] = $input["end"]." ".date("H:m:s", strtotime($input["end"]));

        $input["color"] = "#ffffff";
        Compromissos::create($input);

        return redirect("/site.home");

    }

  }
